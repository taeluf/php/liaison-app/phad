# Phad 2
I'm thinking about starting a version 2 that would have a new template structure. I find the current templates very confusing & hard to work with. I would like a more structured approach that is easier to interact with, diagnose, and modify.

Part of my problem is that right now, I'm thinking about switching to BigDb as an overall data layer for phad, but changing that in code sounds atrocious.
So what are my pain points with phad currently:
- Working with a compiled phad template is confusing. 
- It's hard to understand what is supposed to happen with different p-data nodes
- It's hard to inspect a compiled item because it is unstructured
- It's hard to modify the dom compilation from its current form
- I don't know how to switch out data layers in a nice/clean way.
- Form validation is confusing & too integrated
- ItemInfo should be a defined class, probably


I'm trying to come up with a way to work WITH my current system, via a nice wrapper or more minor changes... so I don't have to do any major refactors.

Right now the `Item` class is for PHAD to use compiling & loading items. It is NOT really intended for the developer to use to inspect the item. It does have some features for that purpose, but they are confusing to use, since it's part of this massive class.

So, I want to add a new class with a friendly interface for users of this library.

Then there is the `ItemInfo` catastrophe. 

Then there is also the database rows/items that are built for displaying.

And the data loading... which isn't that bad, probably, if I make a nice API wrapper.

Oh, phad is also a nightmare to instantiate properly & completely.
