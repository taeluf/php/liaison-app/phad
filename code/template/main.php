<?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */


/* @@if1.start.route_info */
//these two blocks for route & sitemap are not actually used 
// ugh ... i am manually inserting the code instead ...
// idk why, it's just what i did
if ($phad_block==\Phad\Blocks::ROUTE_META){
    return @$$route_info; # route info gets passed into the template
}
/* @@end.route_info */

/* @@if1.start.sitemap_info */
//this code is literally never included (feb 1, 2022) ... see note on route_info above
if ($phad_block==\Phad\Blocks::SITEMAP_META){
    return @$$sitemap_info;
}
/* @@end.sitemap_info */

## becomes $BlogInfo
@$$ItemInfo = (object)[
    'name'=> '/* @@item_name */', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> @$$apis, //rawdata generally json. could be sql or csv though
    'type'=>'/* @@item_type */', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => @$$DataNodes, // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    /* @@candelete */
    /* @@diddelete */
    /* @@cansubmit */
    /* @@FormTarget */
];

/* @@start.form_properties */
@$$ItemInfo->properties = 
    @$$form_properties_array;

/* @@end.form_properties */

$phad->item_initialized(@$$ItemInfo);


/* @@start.form_is_candelete */
if ($phad_block == \Phad\Blocks::FORM_DELETE){
    if ($phad->can_delete(@$$ItemInfo)){
        /* @@form_will_delete */
        $did_delete = $phad->delete(@$$ItemInfo);
        if ($did_delete){
            /* @@form_did_delete */
            $phad->did_delete(@$$ItemInfo);
            return;
        }
    }
}
/* @@end.form_is_candelete */


if ($phad_block==\Phad\Blocks::ITEM_META){
    return @$$ItemInfo;
}

foreach (@$$ItemInfo->data_nodes as $_index=>&$_node){
    $_node['index'] = $_index;
    if (isset($args[':data'])){
        if (!isset($_node['name'])||$_node['name']!=$args[':data']){
            continue;
        }
    }
    if (isset($_node['if'])){
        //@todo templatize p-data 'if' attribute instead of using `eval()`
        $eval_code = 'return ('.$_node['if'].');';
        $result = eval($eval_code);
        if (!$result){
            $_node['response_code'] = 403;
            @$$ItemInfo->response_code = 403;
            continue;
        }
    }
    if (!$phad->can_read_data($_node, @$$ItemInfo)){
        @$$ItemInfo->response_code = 403;
        $_node['response_code'] = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    @$$ItemInfo->rows = $phad->read_data($_node, @$$ItemInfo);
    if (count(@$$ItemInfo->rows)==0){
        @$$ItemInfo->response_code = 404;
        $_node['response_code'] = 404;
        continue;
    }
    @$$ItemInfo->response_code = 200;
    $_node['response_code'] = 200;
    @$$ItemInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows(@$$ItemInfo);
}

/* @@start.status_codes */
if (isset($_node['response_code'])){
    if (@$$ItemInfo->response_code == 200){
        /* @@code_for_response_200 */
    } else {
        foreach (@$$ItemInfo->data_nodes as $_index=>$_node){
            /* @@if2.start.response_code_403 */
            if ($_node['response_code'] == 403){
                /* @@code_for_response_403 */
            }
            /* @@end.response_code_403 */
            /* @@if2.start.response_code_404 */
            if ($_node['response_code'] == 404){
                /* @@code_for_response_404 */
            }
            /* @@end.response_code_404 */
            /* @@if2.start.response_code_500 */
            if ($_node['response_code'] == 500 ){
                /* @@code_for_response_500 */
            }
            /* @@end.response_code_500 */
        }
    }
}
/* @@end.status_codes */


if (count(@$$ItemInfo->rows)===0){
    $phad->no_rows_loaded(@$$ItemInfo);
}

foreach (@$$ItemInfo->rows as $RowIndex_/* @@ItemForeach */ ): 
    @$$ItemName = $phad->object_from_row(@$$Item_Row, @$$ItemInfo);
    /* @@start.form_submit */
        if (@$$ItemInfo->mode == \Phad\Blocks::FORM_SUBMIT){
            // <onsubmit> code goes here. Set `@$$ItemInfo->mode = null` stop submission & display the form.
            /* @@form_on_submit */
            if (@$$ItemInfo->mode==\Phad\Blocks::FORM_SUBMIT){
                if ($phad->can_submit(@$$ItemInfo, @$$Item_Row)
                    &&$phad->submit(@$$ItemInfo, @$$Item_Row)){
                    /* @@form_did_submit */
                    $phad->redirect(@$$ItemInfo,@$$Item_Row);
                    return;
                } else {
                    /* @@form_fail_submit */
                }
            }
            $args['@$$ItemInfoSubmitErrorsList'] = @$$ItemInfo->submit_errors;

            // @$$ItemInfoSubmitErrors = @$$ItemInfo->
            // print_r($args);
        }
    /* @@end.form_submit */

    /* @@start.can_read_row */
    // this block commented out & planned for later deletion. can_read_row() is now being called during read_data() from within the phad method, rather than from within the compiled view.
    // likely a different method will be added for html-defined can_read_row()
    // if (!$phad->can_read_row(@$$Item_Row, @$$ItemInfo, @$$ItemInfo->name)){
    //     continue;
    // }
    /* @@end.can_read_row */
    ?>/* @@html_code */<?php
endforeach;

?>
