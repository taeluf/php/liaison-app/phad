<?php

namespace Phad;

/**
 * INSERTs INTO / UPDATEs database
 */
class PDOSubmitter {

    public $pdo;
    public \Tlf\LilDb $lildb;
    protected ?int $lastInsertId = null;

    public function __construct(\PDO $pdo=null){
        if ($pdo==null)return;
        $this->pdo = $pdo;
        $this->lildb = new \Tlf\LilDb($pdo);
    }

    public function lastInsertId(){
        return $this->lastInsertId;
    }

    // public function getTable($ItemInfo){
    //     return $ItemInfo->table ?? strtolower($ItemInfo->name);
    // }

    /**
     * @param $table_name the sql table name
     * @param $id the id of the row to delete
     */
    public function delete(string $table_name, int $id){
        return $this->lildb->delete($table_name, ['id'=>$id]);
    }

    /**
     * @param $table_name the sql table name
     * @param $SubmitData the data to insert/update with. if $SubmitData['id'] is set, it will be an UPDATE, else an INSERT
     */
    public function submit(string $table_name, array &$SubmitData){
        $pdo = $this->pdo;

        // print_r($SubmitData);
        // exit;
        if (isset($SubmitData['id'])&&is_numeric($SubmitData['id'])){
            $SubmitData['id']  = (int)$SubmitData['id'];
            $this->lildb->update($table_name, $SubmitData);
            // echo 'update me!';
        } else {
            unset($SubmitData['id']);
            $this->lastInsertId = $this->lildb->insert($table_name, $SubmitData);
        // echo 'insert me!';
        // exit;
        }



        return true;
    }

    
    /**
     * Upload a file
     * @param $file an entry in `$_FILES[]`
     * @param $destinationFolder directory to upload to
     * @param $validExts array of extensions or an array containing just `*` to accept all file types.
     * @param $maxMB max size of file to allow
     *
     * Function comes from Util class of repo at https://gitlab.com/taeluf/php/php-utilities
     */
    static public function uploadFile($file, $destinationFolder, $validExts = ['jpg', 'png'], $maxMB = 15)
    {
        // print_r($file);
        // exit;
        if (!is_array($file) || $file == []
            || $file['size'] == 0
            || $file['name'] == ''
            || $file['tmp_name'] == ''
            || !is_int($file['error'])) {
            return false;
        }

        try {
            if (!isset($file['error']) ||
                is_array($file['error'])
            ) {
                throw new \RuntimeException('Invalid parameters.');
            }

            switch ($file['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new \RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new \RuntimeException('Exceeded filesize limit.');
                default:
                    throw new \RuntimeException('Unknown errors.');
            }

            // You should also check filesize here.
            if ($file['size'] > ($maxMB * 1024 * 1024)) {
                throw new \RuntimeException('Exceeded filesize limit.');
            }

            $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
            $ext = strtolower($ext);

            // check file format
            if (!in_array('*', $validExts)&&!in_array($ext, $validExts)) {
                // var_dump($ext);
                // var_dump($validExts);
                // var_dump($file);
                // exit;
                throw new \RuntimeException('Invalid file format ('.$ext.').');
            }

            if (!file_exists($destinationFolder)) {
                mkdir($destinationFolder, 0775, true);
            }

            $fileName = sha1_file($file['tmp_name']) .'-'. uniqid() . '.' . $ext;
            if (!move_uploaded_file(
                $file['tmp_name'],
                $destinationFolder . '/' . $fileName
            )
            ) {
                throw new \RuntimeException('Failed to move uploaded file.');
            }

            return $fileName;

        } catch (\RuntimeException $e) {

            throw $e;

        }
    }
}
