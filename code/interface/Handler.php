<?php

namespace Phad;

interface Handler {
    
    /**
     * Check if a user has a role
     *
     * @param $role a string role, as specified in the `access` attribute of a node
     *
     * @return true or false
     */
    public function user_has_role(string $role): bool;

    /**
     * Check if the current row can be read by the current user. This is NOT called by default when displaying HTML. 
     * This is called when retrieving data from an item (not a view), or when displaying the item html if you set `can_read_row` attribute on an item node. 
     *
     * @param $ItemRow the data being read
     * @param $ItemInfo meta info about the item
     * @param $ItemName the item's name, as defined in the `item` attribute of a node
     *
     * @return true or false
     */
    public function can_read_row(array $ItemRow, object $ItemInfo, string $ItemName);
} 
