<?php

if (!$phad->userAccess->is_admin($lia)){
    echo "Nope";
    return;
}
?>
<h1>Admin Profile</h1>


<p><a href="/user/profile/?show_standard_profile=true">Regular Profile</a></p>

<?php if ($phad->userAccess->is_user_manager($lia)):?>
    <p><a href="/admin/user/">Manage Users</a></p>
<?php endif; ?>


<?php if ($phad->userAccess->is_it_manager($lia) && $phad->initAccess->are_init_pages_enabled($lia)):?>
    <p><a href="/admin/initial-setup/">Create User, Initialize Server</a></p>
<?php endif; ?>
