<?php

$user = $lia->user;

if (!$phad->userAccess->is_admin($lia)||!$phad->userAccess->is_user_manager($lia)){
    echo "nope";
    return;
}

$ldb = new \Tlf\LilDb($lia->pdo);

$which = $_POST['which'];

if ($which=="new_role"){
    $role = $_POST['role'];
    $user->add_role($role);
    echo "Role '$role' added";
} else if ($which=="add_role_to_user"){
    $role = $_POST['role'];
    $email = $_POST['user_email'];
    $lib = new \Tlf\User\Lib($lia->pdo);
    $user = $lib->user_from_email($email);
    $user->add_role($role);
    echo "Role '$role' added to user '$email'";
} else if ($which=="view_roles_of_user"){
    $email = $_POST['user_email'];
    $sql = "SELECT `role` FROM `user_role` ur JOIN `user` u ON ur.user_id = u.id WHERE u.email=:email";
    $results = $ldb->query($sql, ['email'=>$email]);
    echo <<<HTML
        <form action="" method="POST">
            <label>Role To Remove from $email<br>
                <select name="role"><option disabled selected>Select One</option><br>
    HTML;
        foreach ($results as $row){
            $role = $row['role'];
            echo "<option value=\"$role\">$role</option><br>";
        }
    echo '</select></label>';
    echo '<br><br>';

    echo <<<HTML
        <input type="submit" value="Remove Role">
        <input type="hidden" name="user_email" value="$email">
        <input type="hidden" name="which" value="remove_role_from_user">
    HTML;

} else if ($which=='remove_role_from_user'){
    $email = $_POST['user_email'];
    $role = $_POST['role'];
    $lib = new \Tlf\User\Lib($lia->pdo);
    $user = $lib->user_from_email($email);
    $user->remove_role($role);
    echo "Role '$role' removed from user '$email'";
}
