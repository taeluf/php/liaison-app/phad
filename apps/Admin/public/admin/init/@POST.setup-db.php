<?php

if (!$phad->initAccess->are_init_pages_enabled($lia)){
    echo "This page is disabled.";
    return;
}

$file = dirname(__DIR__,5).'/.env/secret.json';
if (file_exists($file)){
    $content = file_get_contents($file);
    $data = json_decode($content, true);
    if (!is_array($data))$data = [];
} else $data = [];

$post = $_POST;

$data['mysql.host'] = $post['host'];
$data['mysql.dbname'] = $post['database'];
$data['mysql.user'] = $post['user'];
$data['mysql.password'] = $post['password'];


file_put_contents($file, json_encode($data, JSON_PRETTY_PRINT));

echo "\n<br>";
echo file_get_contents($file);

echo "<br>Hopefully DB was setup successfully";

?>


