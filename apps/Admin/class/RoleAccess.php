<?php

namespace Phad\App\Admin;

class RoleAccess implements UserAccess {

    public function is_admin($lia): bool{
        if (isset($lia->props['user'])
            &&$lia->user->has_role('admin')
        ){
            return true;
        }

        return false;
    }

    /**
     *
     */
    public function is_user_manager($lia): bool{
        if (isset($lia->props['user'])
            &&$lia->user->has_role('user_manager')
            || $this->is_admin($lia)
        ){
            return true;
        }

        return false;
    }

    /**
     *
     */
    public function is_it_manager($lia): bool{
        if (isset($lia->props['user'])
            &&$lia->user->has_role('it_manager')
        ){
            return true;
        }

        return false;
    }

}
