<?php

namespace Phad\App\Admin;

class DebugModeAccess implements InitAccess {

    public function are_init_pages_enabled($lia): bool{
        if ($lia->debug === true)return true;
        return false;
    }

}
