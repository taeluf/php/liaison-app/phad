<?php 
if (($phad_block??null)===\Phad\Blocks::SITEMAP_META){
    return array (
);
}
?><?php 
if ($phad_block===\Phad\Blocks::ROUTE_META){
    return array (
  0 => 
  array (
    'pattern' => '/admin/initial-setup/',
  ),
);
}
?>

<?php

if (!$phad->initAccess->are_init_pages_enabled($lia)){
    echo "Nope";
    exit;

}

?>

Initial setup

<details><summary>Setup Database</summary>
<form action="/admin/init/setup-db/" method="POST">
    <label>Host<br>
        <input type="text" name="host">
    </label><br>
    <label>Mysql User<br>
        <input type="text" name="user">
    </label><br>
    <label>Mysql Password<br>
        <input type="text" name="password">
    </label><br>
    <label>Mysql Database Name<br>
        <input type="text" name="database">
    </label><br>
    <input type="submit" value="Submit">
</form>
</details>

<details><summary>Init User DB</summary>
    <form action="/admin/init/user-db/" method="POST">
        <label>Web Address<br>
            <input type="text" name="web_address">
        </label><br>
        <label>Email From<br>
            <input type="text" name="email_from">
        </label><br>
            
        <input type="submit" value="Initialize User Database">
    </form>
</details>

<details><summary>Setup Admin User</summary>
    <form action="/admin/init/admin-user/" method="POST">

        <label>Email<br>
            <input type="text" name="email">
        </label><br>

        <label>Password<br>
            <input type="password" name="password">
        </label><br>

        <input type="submit" value="Create Admin User">
    </form>
</details>
