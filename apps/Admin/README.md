# Admin GUI
This is a Liaison + Phad Site Admin GUI. 

## Setup
Setup Lia\Simple, of `taeluf/liaison` and then load the admin app
`deliver.php`:
```php
<?php

    $admin_app_dir = __DIR__.'/vendor/taeluf/phad/apps/Admin/';
    // $lia->deliver_files("$admin_app_dir/public-files/");
    $lia->load_apps($admin_app_dir);
```

And configure it. 

`site/config.json`:
```json
    {
        
    }
```

## User Roles
- `admin` can do most everything
- `it_manager` can access init pages, i think
- `user_manager` can modify users


## Dependencies
- Install `taeluf/liaison v0.6.x-dev`
- Install `taeluf/phad v0.4.x-dev`
- Install `taeluf/resource v0.2.x-dev`. Load Configs with `R()->load(__DIR__.'/site/config.json')`;
- Install `"taeluf/js.autowire": "v0.1.x-dev"`. Add JS dependency in php `$lia->addResourceFile(\Tlf\Js\Autowire::filePath());`

## Configs
- `admin.enable_admin_user_profile` optional, set true to use the admin app's user profile page for `/user/profile/`
- `R("admin.class.user_access")` for most access, implementing `Phad\App\Admin\UserAccess`. Default is `RoleAccess`
- `R("admin.class.init_access")` for init pages access, implementing `Phad\App\Admin\InitAccess`. Default is `DebugModeAccess`, and `Phad\App\Store\RoleInitAccess` is provided to allow a user with role=`it_manager` to have access to init pages

## Routes
- `/admin/`: Main admin page, for links to different admin features
- `/admin/initial-setup/`: Setup database configs, Initialize the user database, Setup the admin user
