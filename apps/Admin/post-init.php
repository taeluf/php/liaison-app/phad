<?php


$app->public_file_params['phad'] = $app->phad;



$phad = $app->phad;
$accessClass= R()->has("admin.class.init_access") 
                ? R("admin.class.init_access") 
                : "\\Phad\\App\\Admin\\DebugModeAccess";
$access = new $accessClass($lia, $phad);

$phad->setInitAccess($access);



$phad = $app->phad;
$accessClass= R()->has("admin.class.user_access") 
                ? R("admin.class.user_access") 
                : "\\Phad\\App\\Admin\\RoleAccess";
$access = new $accessClass($lia, $phad);

$phad->setUserAccess($access);




$admin_profile_enabled = false;
if (R()->has("admin.enable_admin_user_profile") && R("admin.enable_admin_user_profile")===true) {
    $admin_profile_enabled = true;
}


if ($admin_profile_enabled && $lia->user->has_role('admin')){

    $file = __DIR__.'/admin-routes/user_profile.php';
    $lia->addRoute('/user/profile/', $file, $app);

    $lia->hook('FilterRoute', 
        function(\Lia\Obj\Route $route) use ($app, $lia, $file){
            if ($route->url()!='/user/profile/')return true;

            $is_admin = $app->phad->userAccess->is_admin($lia);

            // echo __DIR__.'/admin-routes/user_profile.php';
            // echo realpath($route->target());
            // exit;
            $is_our_profile = 
                is_string($route->target())
                && realpath($route->target())==realpath(__DIR__.'/admin-routes/user_profile.php')
                // in case realpath returns an empty string or falsey value
                && realpath($route->target()) != '';

            $is_requesting_standard_profile = 
                isset($_GET['show_standard_profile'])
                && $_GET['show_standard_profile'] = 'true';

            // var_dump($route->target());
            // var_dump("Is_our_profile:".$is_our_profile);
            // var_dump("Is_admin:".$is_admin)
            // exit;


            if ($is_requesting_standard_profile && $is_our_profile) return false;
            else if ($is_requesting_standard_profile && !$is_our_profile) return true;
            if (!$is_admin && $is_our_profile){
                return false;
            } 

            $can_show_this_route = $is_admin && $is_our_profile;

            // var_dump($can_show_this_route);
//
                // echo 'fail here';
                // exit;
            return $can_show_this_route;

        }
    );

}


