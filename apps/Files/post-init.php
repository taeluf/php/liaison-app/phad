<?php

// var_dump(array_keys(get_defined_vars()));
// exit;
//

$phad = $app->phad;
$phad->dir_upload = R("files.upload_dir");

$accessClass= R()->has("files.class.data_access") 
                ? R("files.class.data_access") 
                : "\\Tlf\\Phad\\Files\\AdminAccess";
$dataAccess = new $accessClass($lia, $phad);

$phad->setDataAccess($dataAccess);

$app->phad->data_loaders['dv:get_files'] = 
    function($node, $ItemInfo) use ($lia){
        $ldb = $lia->ldb;
        return $ldb->select('file');
    };

// $app->phad->access_handlers['dv.can_view_files'] =
    // function ($data_node_info, $ItemInfo) use ($lia){
        // if ($lia->user->has_role('admin'))return true;
        // else {
            // throw new \Exception("Cannot View page because not an admin.");
        // }
//
        // return false;
    // };
//
// $app->phad->access_handlers['dv.can_edit_file'] =
    // function ($ItemInfo, $RowToStore) use ($lia){
        // if ($lia->user->has_role('admin'))return true;
        // else {
            // throw new \Exception("Cannot Edit File.");
        // }
//
        // return false;
    // };
//
//
// $app->phad->access_handlers['dv.can_delete_file'] =
    // function ($ItemInfo) use ($lia){
        // if ($lia->user->has_role('admin'))return true;
        // else {
            // throw new \Exception("Cannot Delete File.");
        // }
//
        // return false;
    // };
//
// $app->phad->
