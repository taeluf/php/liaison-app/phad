<?php
/**
 * GET /files/search/ display search form
 * GET /files/search/?q=whatever perform a search & get a JSON response
 * POST /files/search/ [selected_id=$id & selected_url=$url], echo {url:$url, id:$id}
 *
 */


    // print json for completed selection
    if (isset($_POST['selected_id'])){
        echo json_encode(
            ['id'=>$_POST['selected_id'],'url'=>$_POST['selected_url']]
        );
        exit;
    }

    // query for a file
    if (isset($_GET['q'])){
        $stmt = $lia->pdo->prepare(
            "SELECT * from file 
                WHERE `is_public` = 1 
                AND (lookup_key LIKE :query OR download_name LIKE :query OR alt_text LIKE :query)
                ORDER BY
                  CASE
                    WHEN lookup_key LIKE :query THEN 1
                    WHEN download_name LIKE :query THEN 2
                    WHEN alt_text LIKE :query THEN 3
                    ELSE 4
                  END
                LIMIT 0,10
            ");

        $stmt->execute([
            'query' => '%'.$_GET['q'].'%',
        ]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // convert the rows into url/id only
        $final_rows = [];
        foreach ($rows as $row){
            $obj = new \Dv\File\Db\File($row);
            $final_rows[] = [
                'url'=>$obj->url,
                'id'=>$obj->id
            ];
        }

        echo json_encode($final_rows);

        exit;
    }

    // add as url because compilation just causes problems here
    $lia->addResourceUrl('/files/search.js');
?>


<div class="PhotoSearch">
<form action="/files/search/" method="POST">
<label>Search for Photo
    <input type="text" name="query" placeholder="Search for file" onkeyup="conduct_photo_search(this)"/>
    <input type="hidden" name="selected_id">
    <input type="hidden" name="selected_url">
</label>

    <style type="text/css">
        
        .PhotoSearchResults img {
            max-width:75px;
            max-height:75px;
            border: 1px solid rgba(0,0,0,0.2);
            margin:1px;
            cursor:pointer;
        }
    </style>
    <div class="PhotoSearchResults" onclick="photo_clicked(event,this)">
    </div>

    <input type="submit" value="Submit" style="display:none;">
</form>

</div>
<hr>

