
async function show_file(n){
    // console.log(n);
    // console.log(n.files);
    const img = n.parentNode.querySelector('img');
    // img.src = n.value;
    // img.style.visibility = "hidden";
    // img.style.visibility = "normal";
    var reader = new FileReader(); //Initialize FileReader.
    reader.onload = function(e){
        img.src = e.target.result;
    }
    reader.readAsDataURL(n.files[0]);
    // console.log(n.files[0]);
    // console.log(n.files);
    // console.log(n);
    // const result = await reader.readAsDataURL(n.files[0]);
    // img.src = result;
}
var uploaded_files = [];
window.addEventListener('paste',
    async function (e){
        const form = document.querySelector('form[action="/files/upload/"]');
        const file_input = form.querySelector('input[type="file"]');
        // console.log(form);
        // console.log(file_input);
        const err = form.querySelector('.file_input_error');
        // console.log(e.clipboardData);
        // console.log(e.clipboardData.getData('files'));
        if (e.clipboardData.files.length == 0){
            err.innerText = 'No images in the clipboard. Copy an image and try again.';
            err.style.display = "block";
            return;
        }
        err.style.display = "none";

        file_input.files = e.clipboardData.files;
        show_file(file_input);
        const changeEvent = new Event("change");
        file_input.dispatchEvent(changeEvent);
        // console.log(file_input);
        // file_input.onchange();
    }
);


// return;
// const fileInput = document.getElementById("document_attachment_doc");
//
// fileInput.addEventListener('change', () => {
  // form.submit();
// });
//
// window.addEventListener('paste', e => {
  // fileInput.files = e.clipboardData.files;
// });
