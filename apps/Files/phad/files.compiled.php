<?php 
if (($phad_block??null)===\Phad\Blocks::SITEMAP_META){
    return array (
);
}
?><?php 
if ($phad_block===\Phad\Blocks::ROUTE_META){
    return array (
  0 => 
  array (
    'pattern' => '/files/',
  ),
);
}
?>

<a href="/files/upload/"><button>Upload File</button></a>
<h1>Files</h1>
<section class="Files">
    <?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */






## becomes $BlogInfo
$fileInfo = (object)[
    'name'=> 'file', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> array (
  0 => 'view',
  1 => 'data',
), //rawdata generally json. could be sql or csv though
    'type'=>'view', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => array (
  0 => 
  array (
    'data_loader' => 'dv:get_files',
    'access' => 'call:dv.can_view_files',
    'type' => 'node',
  ),
  1 => 
  array (
    'type' => 'default',
  ),
), // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    
    
    
    
];



$phad->item_initialized($fileInfo);





if ($phad_block==\Phad\Blocks::ITEM_META){
    return $fileInfo;
}

foreach ($fileInfo->data_nodes as $_index=>&$_node){
    $_node['index'] = $_index;
    if (isset($args[':data'])){
        if (!isset($_node['name'])||$_node['name']!=$args[':data']){
            continue;
        }
    }
    if (isset($_node['if'])){
        //@todo templatize p-data 'if' attribute instead of using `eval()`
        $eval_code = 'return ('.$_node['if'].');';
        $result = eval($eval_code);
        if (!$result){
            $_node['response_code'] = 403;
            $fileInfo->response_code = 403;
            continue;
        }
    }
    if (!$phad->can_read_data($_node, $fileInfo)){
        $fileInfo->response_code = 403;
        $_node['response_code'] = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    $fileInfo->rows = $phad->read_data($_node, $fileInfo);
    if (count($fileInfo->rows)==0){
        $fileInfo->response_code = 404;
        $_node['response_code'] = 404;
        continue;
    }
    $fileInfo->response_code = 200;
    $_node['response_code'] = 200;
    $fileInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows($fileInfo);
}




if (count($fileInfo->rows)===0){
    $phad->no_rows_loaded($fileInfo);
}

foreach ($fileInfo->rows as $RowIndex_file => $fileRow ): 
    $file = $phad->object_from_row($fileRow, $fileInfo);
    

    
    ?><div>
        
        <a class="img" href="<?=$file->editUrl?>">
            <img src="<?=$file->thumb_url?>" alt="<?=$file->alt_text?>">
        </a>
        <span>
            <a href="<?=$file->downloadUrl?>"><small><?=$file->download_name?></small></a>
            <?php if ($file->lookup_key!=null&&$file->lookup_key!=''):  ?>
                <br>
                <a if="" href="<?=$file->url?>">@<small><?=$file->lookup_key?></small></a>
            <?php endif;?>
        </span>
    </div><?php
endforeach;

?>

</section>
