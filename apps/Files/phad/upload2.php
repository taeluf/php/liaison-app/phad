<route pattern="/files/upload-v2/"></route>
<?php
/**
 * POST /files/upload/ with field -response_type-=json to get a json response on submission
 * GET /files/upload/?status=public to get form with `is_public` set to true.
 */

$lia->addResourceUrl('/files/search2.js');

if (!$phad->dataAccess->can_view_files([])){
    http_response_code(403);
    echo "<h1>Not Allowed</h1>";
    return;

}
?>


<div class="FilesUpload">
<p><a href="/files/">Go To Files</a></p>
<br>

<div class="PhotoSearch" access="call:dv.can_edit_file">
<form action="/files/search/" method="POST">
<label>Search<br>
    <input type="text" name="query" placeholder="Search for file" onkeyup="conduct_photo_search(this)"/>
    <input type="hidden" name="selected_id">
    <input type="hidden" name="selected_url">
</label>

    <style type="text/css">
        
        .PhotoSearchResults img {
            width:100px;
            max-height:100px;
            border: 1px solid rgba(0,0,0,0.2);
            margin:1px;
            cursor:pointer;
        }
    </style>
    <div class="PhotoSearchResults" onclick="photo_clicked(event,this)">
    </div>

    <input type="submit" value="Submit" style="display:none;">
</form>
</div>
<br>

<h2>Upload File</h2>
<form item="File" target="/files/" action="/files/upload/"
    candelete="call:dv.can_delete_file" cansubmit="call:dv.can_edit_file"
>
  <p-data where="id = :id " access="call:dv.can_edit_file"></p-data>
  <p-data where="id = :get.id " access="call:dv.can_edit_file"></p-data>
  <on s=403><p>You're not allowed to view this page.</p></on>

<willdelete><?php
    // delete the file on disk
    echo "<p>File deleted!</p>";
    echo "<p><a href=\"/files/\">View Files</a></p>";
    $file = $_GET['id'];
    $file = \Dv\File\Phad::get_file_by_id($lia->pdo, (int)$file);
    $path = $file->path;
    if (file_exists($path))unlink($path);
?></willdelete>
<onsubmit><?php
    // do the file upload & set the file type & download name
    // print_r($FileRow);
    // exit;
    $phad->upload_file('file', $FileInfo, $FileRow, 'stored_name');
    if (!isset($FileRow['id'])&&!isset($FileRow['stored_name'])){
        // phpinfo();
        // exit;
        echo "There was a problem with the upload. Possibly the file was too large? I don't know.";
        return;
    }
    if (isset($FileRow['stored_name'])){
        $FileRow['file_type'] = pathinfo($FileRow['stored_name'],PATHINFO_EXTENSION);
    }
    if ($FileRow['lookup_key']=='')$FileRow['lookup_key'] = null;
    if (strlen(trim($FileRow['download_name']))==0)$FileRow['download_name'] = $_FILES['file']['name'] ?? 'no-name';

    unset($FileRow['-response_type-']);
?></onsubmit>
    <didsubmit><?php
        if (isset($_POST['-response_type-'])&&$_POST['-response_type-']=='json'){
            ob_get_clean();
            $item = $lia->phad('upload', ['id'=>$FileRow['id']]);
            $data = $item->rows()[0];
            $obj = new \Dv\File\Db\File($data);
            $data['url'] = $obj->thumb_url;
            // echo $item;
            echo json_encode($data);
            // echo json_encode($FileRow);
            exit;
        }
    ?></didsubmit>

    <errors></errors>

    <label>Name<br>
        <input type="text" name="download_name" />
    </label><br>
    <label>Alt Text <br>
        <textarea name="alt_text" maxlength="1000"></textarea>
    </label><br>
<br>
    <label>Status
        <select name="is_public" required>
            <option value="0">Private</option>
            <option value="1" <?php if (isset($_GET['status'])&&$_GET['status']=='public') echo 'selected';?>>Public</option>
    </label>
<br>
<br>
    <label style="cursor:pointer;">File<br>
        <input type="file" name="file" style="display:none;" onchange="show_file(this)"/>
        <div class="file_input_error error" style="display:none"></div>
        <div style="display:inline-block;padding:16px;border:1px solid rgba(0,0,0,0.3);">
            <img src="<?=$File->thumb_url?>" alt="Upload File" style="display:block;max-width:200px;max-height:200px;"/>
        </div>
    </label><br>

    <br><br>
    <input type="submit" value="Submit" />
    <br>

    <input type="hidden" name="id" />

    <input type="backend" name="file_type"/> 

</form>

</div>
