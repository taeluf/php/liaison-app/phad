<?php 
if (($phad_block??null)===\Phad\Blocks::SITEMAP_META){
    return array (
);
}
?><?php 
if ($phad_block===\Phad\Blocks::ROUTE_META){
    return array (
  0 => 
  array (
    'pattern' => '/files/upload-v2/',
  ),
);
}
?>
<?php
/**
 * POST /files/upload/ with field -response_type-=json to get a json response on submission
 * GET /files/upload/?status=public to get form with `is_public` set to true.
 */

$lia->addResourceUrl('/files/search2.js');

if (!$phad->dataAccess->can_view_files([])){
    http_response_code(403);
    echo "<h1>Not Allowed</h1>";
    return;

}
?>


<div class="FilesUpload">
<p><a href="/files/">Go To Files</a></p>
<br>

<?php if ($phad->can_read_node(array (
  'class' => 'PhotoSearch',
  'access' => 'call:dv.can_edit_file',
  'tagName' => 'div',
))): ?>
<div class="PhotoSearch">
<form action="/files/search/" method="POST">
<label>Search<br>
    <input type="text" name="query" placeholder="Search for file" onkeyup="conduct_photo_search(this)">
    <input type="hidden" name="selected_id">
    <input type="hidden" name="selected_url">
</label>

    <style type="text/css">
        
        .PhotoSearchResults img {
            width:100px;
            max-height:100px;
            border: 1px solid rgba(0,0,0,0.2);
            margin:1px;
            cursor:pointer;
        }
    </style>
    <div class="PhotoSearchResults" onclick="photo_clicked(event,this)">
    </div>

    <input type="submit" value="Submit" style="display:none;">
</form>
</div>
<?php else: $phad->read_node_failed(array (
  'class' => 'PhotoSearch',
  'access' => 'call:dv.can_edit_file',
  'tagName' => 'div',
)); ?>
<?php endif; ?>
<br>

<h2>Upload File</h2>
<?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */






## becomes $BlogInfo
$FileInfo = (object)[
    'name'=> 'File', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> array (
  0 => 'form',
  1 => 'create',
  2 => 'update',
), //rawdata generally json. could be sql or csv though
    'type'=>'form', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => array (
  0 => 
  array (
    'where' => 'id = :id ',
    'access' => 'call:dv.can_edit_file',
    'type' => 'node',
  ),
  1 => 
  array (
    'where' => 'id = :get.id ',
    'access' => 'call:dv.can_edit_file',
    'type' => 'node',
  ),
  2 => 
  array (
    'type' => 'default',
  ),
), // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    'candelete' => 'call:dv.can_delete_file',
    'diddelete' => false,
    'cansubmit' => 'call:dv.can_edit_file',
    'target' => '/files/',
];


$FileInfo->properties = 
    array (
  'download_name' => 
  array (
    'type' => 'text',
    'tagName' => 'input',
  ),
  'alt_text' => 
  array (
    'maxlength' => '1000',
    'tagName' => 'textarea',
  ),
  'is_public' => 
  array (
    'required' => '',
    'tagName' => 'select',
    'options' => 
    array (
      0 => '0',
      1 => '1',
    ),
  ),
  'file' => 
  array (
    'type' => 'file',
    'style' => 'display:none;',
    'onchange' => 'show_file(this)',
    'tagName' => 'input',
  ),
  'id' => 
  array (
    'type' => 'hidden',
    'tagName' => 'input',
  ),
  'file_type' => 
  array (
    'type' => 'backend',
    'tagName' => 'input',
  ),
);



$phad->item_initialized($FileInfo);



if ($phad_block == \Phad\Blocks::FORM_DELETE){
    if ($phad->can_delete($FileInfo)){
        ?><?php
            // delete the file on disk
            echo "<p>File deleted!</p>";
            echo "<p><a href=\"/files/\">View Files</a></p>";
            $file = $_GET['id'];
            $file = \Dv\File\Phad::get_file_by_id($lia->pdo, (int)$file);
            $path = $file->path;
            if (file_exists($path))unlink($path);
        ?><?php
        $did_delete = $phad->delete($FileInfo);
        if ($did_delete){
            
            $phad->did_delete($FileInfo);
            return;
        }
    }
}



if ($phad_block==\Phad\Blocks::ITEM_META){
    return $FileInfo;
}

foreach ($FileInfo->data_nodes as $_index=>&$_node){
    $_node['index'] = $_index;
    if (isset($args[':data'])){
        if (!isset($_node['name'])||$_node['name']!=$args[':data']){
            continue;
        }
    }
    if (isset($_node['if'])){
        //@todo templatize p-data 'if' attribute instead of using `eval()`
        $eval_code = 'return ('.$_node['if'].');';
        $result = eval($eval_code);
        if (!$result){
            $_node['response_code'] = 403;
            $FileInfo->response_code = 403;
            continue;
        }
    }
    if (!$phad->can_read_data($_node, $FileInfo)){
        $FileInfo->response_code = 403;
        $_node['response_code'] = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    $FileInfo->rows = $phad->read_data($_node, $FileInfo);
    if (count($FileInfo->rows)==0){
        $FileInfo->response_code = 404;
        $_node['response_code'] = 404;
        continue;
    }
    $FileInfo->response_code = 200;
    $_node['response_code'] = 200;
    $FileInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows($FileInfo);
}


if (isset($_node['response_code'])){
    if ($FileInfo->response_code == 200){
        
    } else {
        foreach ($FileInfo->data_nodes as $_index=>$_node){
            
            if ($_node['response_code'] == 403){
                if ($_index === 0):
    ?><p>You're not allowed to view this page.</p><?php
endif;


            }
            
            
            
        }
    }
}



if (count($FileInfo->rows)===0){
    $phad->no_rows_loaded($FileInfo);
}

foreach ($FileInfo->rows as $RowIndex_File => $FileRow ): 
    $File = $phad->object_from_row($FileRow, $FileInfo);
    
        if ($FileInfo->mode == \Phad\Blocks::FORM_SUBMIT){
            // <onsubmit> code goes here. Set `$FileInfo->mode = null` stop submission & display the form.
            ?><?php
            // do the file upload & set the file type & download name
            // print_r($FileRow);
            // exit;
            $phad->upload_file('file', $FileInfo, $FileRow, 'stored_name');
            if (!isset($FileRow['id'])&&!isset($FileRow['stored_name'])){
                // phpinfo();
                // exit;
                echo "There was a problem with the upload. Possibly the file was too large? I don't know.";
                return;
            }
            if (isset($FileRow['stored_name'])){
                $FileRow['file_type'] = pathinfo($FileRow['stored_name'],PATHINFO_EXTENSION);
            }
            if ($FileRow['lookup_key']=='')$FileRow['lookup_key'] = null;
            if (strlen(trim($FileRow['download_name']))==0)$FileRow['download_name'] = $_FILES['file']['name'] ?? 'no-name';
        
            unset($FileRow['-response_type-']);
        ?><?php
            if ($FileInfo->mode==\Phad\Blocks::FORM_SUBMIT){
                if ($phad->can_submit($FileInfo, $FileRow)
                    &&$phad->submit($FileInfo, $FileRow)){
                    ?><?php
                if (isset($_POST['-response_type-'])&&$_POST['-response_type-']=='json'){
                    ob_get_clean();
                    $item = $lia->phad('upload', ['id'=>$FileRow['id']]);
                    $data = $item->rows()[0];
                    $obj = new \Dv\File\Db\File($data);
                    $data['url'] = $obj->thumb_url;
                    // echo $item;
                    echo json_encode($data);
                    // echo json_encode($FileRow);
                    exit;
                }
            ?><?php
                    $phad->redirect($FileInfo,$FileRow);
                    return;
                } else {
                    
                }
            }
            $args['FileSubmitErrorsList'] = $FileInfo->submit_errors;

            //  = $FileInfo->
            // print_r($args);
        }
    

    
    ?><form action="/files/upload/" enctype="multipart/form-data" method="POST">
  
  
  



    

    <div class="errors">
<?php foreach ($FileInfo->submit_errors as $e){
    echo "\n        <p>".$e['msg']."</p>";
}?>  
</div>


    <label>Name<br>
        <input type="text" name="download_name" value="<?=$File->download_name?>">
    </label><br>
    <label>Alt Text <br>
        <textarea name="alt_text" maxlength="1000"><?=$File->alt_text?></textarea>
    </label><br>
<br>
    <label>Status
        <select name="is_public" required>
            <option value="0" <?=('0'==$File->is_public)? ' selected="" ' : ' '?>>Private</option>
            <option value="1" <?php if (isset($_GET['status'])&&$_GET['status']=='public') echo 'selected';?> <?=('1'==$File->is_public)? ' selected="" ' : ' '?>>Public</option>
    </select></label>
<br>
<br>
    <label style="cursor:pointer;">File<br>
        <input type="file" name="file" style="display:none;" onchange="show_file(this)">
        <div class="file_input_error error" style="display:none"></div>
        <div style="display:inline-block;padding:16px;border:1px solid rgba(0,0,0,0.3);">
            <img src="<?=$File->thumb_url?>" alt="Upload File" style="display:block;max-width:200px;max-height:200px;">
        </div>
    </label><br>

    <br><br>
    <input type="submit" value="Submit">
    <br>

    <input type="hidden" name="id" value="<?=$File->id?>">

     

</form><?php
endforeach;

?>


</div>
