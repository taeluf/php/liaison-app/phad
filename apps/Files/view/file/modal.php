<?php
/**
 * Simply call `$lia->view('story/modal');` OUTSIDE of any form nodes to include it on the page.
 *
 * Then create a modal instance, set `modal.content` to a string 
 * and call `modal.handleForm(modal.forms[0], some_function(modal, response_text))`
 * then call `modal.show()` to display the content.
 *
 * See the view story/file-selector for a good example of this.
 *
 */
?>

<div class="ModalPopup" style="display:none">
    <div class="ModalDialog">
        <button class="DialogX">X</button>
        <h6 class="ModalTitle"></h6>
        <div class="ModalContent">
        </div>
    </div>
</div>
