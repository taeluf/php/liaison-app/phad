<?php

namespace Tlf\Phad\Files;

interface DataAccess {

    public function __construct(\Lia $lia, \Phad $phad);
    public function can_download_file(array $file_row): bool;
    public function can_view_files(array $data_node_info): bool;
    public function can_edit_file($ItemInfo, ?array $RowToStore=null): bool;
    public function can_delete_file($ItemInfo): bool;
    public function can_view_private_files(): bool;
}
