<?php

namespace Tlf\Phad\Files;

class AdminAccess implements DataAccess {

    public \Lia $lia;
    public \Phad $phad;

    public function __construct(\Lia $lia, \Phad $phad){
        $this->lia = $lia;
        $this->phad = $phad;
    }

    /**
     * Check if a file can be downloaded. 
     *
     * @param $file_row I DONT KNOW. It's apparently a liaison instance at least one time
     *
     * @return true if `$lia->user_>has_role('admin')` is true
     */
    public function can_download_file($file_row): bool {
        if ($file_row['is_public']==1)return true;
        if ($this->lia->user->has_role('admin'))return true;

        return false;
    }

    /**
     * Check if all files can be viewed / if a list of files can be viewed
     *
     * @return true if `$lia->user_>has_role('admin')` is true
     */
    public function can_view_files(array $data_node_info): bool{
        $lia = $this->lia;
        if ($lia->user->has_role('admin'))return true;
        else {
            throw new \Exception("Cannot View page because not an admin.");
        }

        return false;
    }

    /**
     * Check if a file can be edited
     *
     * @return true if `$lia->user_>has_role('admin')` is true
     */
    public function can_edit_file($ItemInfo, ?array $RowToStore=null): bool{
        $lia = $this->lia;
        if ($lia->user->has_role('admin'))return true;
        else {
            throw new \Exception("Cannot Edit File.");
        }

        return false;
    }

    /**
     * Check if a file can be deleted
     *
     * @return true if `$lia->user_>has_role('admin')` is true
     */
    public function can_delete_file($ItemInfo): bool{
        $lia = $this->lia;
        if ($lia->user->has_role('admin'))return true;
        else {
            throw new \Exception("Cannot Delete File.");
        }

        return false;
    }

    public function can_view_private_files(): bool {
        $lia = $this->lia;
        if ($lia->user->has_role('admin'))return true;

        return false;
    }
}
