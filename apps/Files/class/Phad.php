<?php

namespace Dv\File;

class Phad extends \Phad {

    /**
     * Absolute path to directory to upload files into
     */
    public string $dir_upload;

    public \Tlf\Phad\Files\DataAccess $dataAccess;

    public function setDataAccess(\Tlf\Phad\Files\DataAccess $dataAccess){
        $this->dataAccess = $dataAccess;
        $this->access_handlers['dv.can_view_files'] = [$dataAccess, 'can_view_files'];
        $this->access_handlers['dv.can_edit_file'] = [$dataAccess, 'can_edit_file'];
        $this->access_handlers['dv.can_delete_file'] = [$dataAccess, 'can_delete_file'];
    }

    public function object_from_row($row, $ItemInfo){
        $orm_obj = $this->orm_obj($row,$ItemInfo->name);
        if ($orm_obj!=false)return $orm_obj;
        return parent::object_from_row($row, $ItemInfo);
    }

    public function orm_obj(array $row,string $name){
        $class = 'Dv\\File\\Db\\'.ucfirst($name);
        if (class_exists($class, true)){
            return new $class($row);
        }

        return false;
    }

    /**
     * Check if the file can be downloaded. If public file yes, if user is an admin, then yes. Otherwise no.
     *
     * @param $file_row an array from the database
     * @return true/false
     */
    public function can_download_file($file_row){
        if ((int)$file_row['is_public']==1)return true;

        // print_r($file_row);

        if ($this->dataAccess->can_download_file($file_row))return true;
        return false;
    }

    /**
     * Get a file based on it's id in the database
     * @param $pdo a pdo object
     * @param $id the id in the database
     * @return a File instance on success or null on failure
     */
    static public function get_file_by_id(\PDO $pdo,int $id){
        $stmt = $pdo->prepare("SELECT * FROM `file` WHERE `id` = :id");
        $stmt->execute(['id'=>$id]);

        $rows = $stmt->fetchAll();
        if (count($rows)!==1){
            return null;
        }
        return new \Dv\File\Db\File($rows[0]);
    }

    /**
     * Upload a file & update the phad vars to make the db insert/update work correctly
     * @param $upload_key the key in `$_FILES[]`
     * @param $ItemInfo the ItemInfo object from the phad item
     * @param &$ItemRow the item row from the phad item which will be inserted into the db
     * @param $column_name the column in the database which should hold the file name
     *
     * @return true if success, false if no upload completed
     */
    public function upload_file(string $upload_key, object $ItemInfo, array &$ItemRow, string $column_name) {
        // var_dump(array_keys(get_object_vars($this)));
        $upload_dir = $this->dir_upload;

        $file_name = \Phad\PDOSubmitter::uploadFile(
                    $_FILES[$upload_key]??[],
                    $upload_dir,['*'],
                    15
                );
        if ($file_name==null){
            // print_r($_FILES);
            // var_dump($upload_dir);
            // var_du
            // echo 'failure!';
            // exit;
            return false;
        }
        if (!isset($ItemInfo->properties[$column_name])){
            $ItemInfo->properties[$column_name] = ['type'=>'backend', 'tagName'=>'input'];
        }
        $ItemRow[$column_name] = $file_name;

        $upload_name = $_FILES[$upload_key]['name']??'no-name';
        
        static::log('uploaded_files', $file_name.'('.$upload_name.'): '.$ItemInfo->name.'('.$ItemRow['id'].','.($ItemRow['name']??'').'), '.$column_name);
        
        return true;
    }

    /**
     * @param $file the file name to log to (`.txt` will be added to it)
     * @param $msg a string to log
     */
    static public function log(string $file, string $msg){
        $path = dirname(__DIR__,1).'/upload-log.txt';
        $date = date("M d, Y H:i:s");
        $fh = fopen($path, 'a');
        fwrite($fh, "\n".$date.'- '.$msg);
        fclose($fh);
    }
}
