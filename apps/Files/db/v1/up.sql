
CREATE TABLE `file` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stored_name` varchar(256) NOT NULL,
  `download_name` varchar(256) NOT NULL,
  `is_public` tinyint NOT NULL DEFAULT 0,
  `alt_text` varchar(1000) NOT NULL DEFAULT '',
  `file_type` varchar(256) NOT NULL,
  `lookup_key` varchar(256) NULL UNIQUE,
  PRIMARY KEY (`id`),
  UNIQUE(`stored_name`)
);
