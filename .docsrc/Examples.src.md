# Examples
Note (feb 4, 2022): These are old examples that i haven't properly reviewed and tested ... the examples are probably accurate, but ... idk


## Write a view
@ast_class(Phad\Test\Documentation,method.testWriteAView.docblock)
```html
@file(test/input/views/Documentation/View/WriteAView.php)
```

### Create a filter
@ast_class(Phad\Test\Documentation,method.testCreateAFilter.docblock)
```php
@import(Filter.add)
```

### Handle Errors
@ast_class(Phad\Test\Documentation, method.testHandleErrors.docblock)
```php
@file(test/input/views/Documentation/View/HandleErrors.php)
```

### Inline Queries
@ast_class(Phad\Test\Documentation, method.testInlineQuery.docblock)
```php
@file(test/input/views/Documentation/View/InlineQueries.php)
```

### Routing
@ast_class(Phad\Test\Documentation, method.testRouting.docblock)
```html
@file(test/input/views/Documentation/View/Routing.php)
```

### Sitemap Generation
@ast_class(Phad\Test\Documentation, method.testSitemapGeneration.docblock)
```html
@file(test/input/views/Documentation/View/Sitemap.php)
```

#### Defining the Sitemap Handler
```php
@import(Sitemap.addHandler)
```

### Control Access
```php
@file(test/input/views/Documentation/View/ControlAccess.php)
```

#### Specify an access node to use
```php
@import(Access.SpecifyName)
```

## Write a form
```php
@file(test/input/views/Documentation/Form/WriteAForm.php)
```

### OnSubmit handler
```php
@file(test/input/views/Documentation/Form/SubmitHandler.php)
```

## Spam Controls
Add this to your form. You can change the string `'contact'` to identify different forms and you can pass an array other than `$_POST` as the submitted data.
```php
    <onsubmit><?php
        $phad->verify_spam_control('contact', $_POST);
    ?></onsubmit>
    <?=$phad->show_spam_control('contact')?>
```


