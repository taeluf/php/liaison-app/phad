# Troubleshooting

## Known issues
- Every custom node requires a `</closing>` tag. i.e. `<route>`, `<access>`, `<x-item>` & any others  
- `Uncaught TypeError: Taeluf\PHTML::insertCodeBefore(): Argument #1 ($node) must be of type DOMNode, null given` - Make sure you don't have any empty `.php` files in your `phad` dir. 
- Commit `912d622` added breaking changes regarding `can_read_row()`. Commit `c3f423b` is the commit before that. If you updated on branch v0.4 and things broke, you can revert to `c3f423b`. Alternatively, make sure your `can_read_row()` handler accepts a `string` as the 3rd argument, AND make sure `can_read_row` return `true` for any node where `can_read_row` was not set on the node. THEN, commit `c6dd271` removes `can_read_row()` from the compiled output & instead checks can_read_row during `read_data()`. This adds overhead of looping over every row before data is returned.
