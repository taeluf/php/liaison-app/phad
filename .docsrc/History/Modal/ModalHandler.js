class ModalHandler extends Autowire {
    get modalName(){return this.n.getAttribute('modal');}
    get modal(){return this.getAny('Modal');}


    onAttach(){
        this.requestParams = [];
        const requestData = this.getRequestParamsFromNode();
        for (const key in requestData){
            this.setRequestParam(key, requestData[key]);
        }
    }

    async onclick(){

        const onRequest = this.onRequest(this.requestParams, this);
        const postData = {modal:this.modalName, ...this.requestParams};
        const modalData = await this.fj('/modal/', postData);
        this.modal.show();
        const formNode = this.modal.setFormHtml(modalData.form);
        const formObj = new ModalForm(formNode, this, onRequest);
        //option 1:
        // set the action on the form
        // set a submission handler
        // set a hidden input giving the table name
        //
        //
        // ultimately, I need PHP to look back at the submitted form
        // So the server needs to know the name of the form, so it can be inspected for properties & requirements
        // I think I should do all the form prep on the server
    }

    onSubmitData(data){
        console.log('parent handler onsubmitdata');
        return data;
    }
    onResponse(responseData, submitData, fromUrl, taskInGui){
        console.log('parent handler onResponse');
    }

    getRequestParamsFromNode(){
        // they have column-param_name="value"
        return [];
    }

    setRequestParam(name, value){
        this.requestParams[name] = value;
    }

    removeRequestParam(name){
        delete this.requestParams[name];
    }

    onRequest(){return [];}
}
