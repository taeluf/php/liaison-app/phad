<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/compilation/TemplateCompiler.php  
  
# class Phad\TemplateCompiler  
Template Compiler for compiling PHP Scripts from a template, a view, and an array of arguments to fill in the template  
See source code at [/code/compilation/TemplateCompiler.php](/code/compilation/TemplateCompiler.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function compile(string $item, string $template): string`   
- `public function precompile_ifs(string $template, array $template_fillers)` Process all `/* @@if.start.name *\/` placeholders. Modifies `$template` and `$template_fillers`  
  
Replaces `@@if1.start.whatever` with `@@start.whatever` and adds `@@__if1_#` before `if()`.  
Adds `__if1_#` entries to `$template_fillers`  
  
- `public function fill_template(string $template, array $template_args, array $template_fillers)` Fill a template (final template compilation step)  
  
- `public function get_template_args(string $template, array $template_fillers)` Get array of arguments found inside the template (which will later be replaced)  
  
- `public function get_start_end_args(string $template, array $template_fillers)` Get args like `/* @@start.name *\/` (and @@end) and modify the template & template fillers as needed  
  
- `public function get_var_args(string $template)` Get template args like `@$$Arg`  
- `public function get_comment_args(string $template)` Get template args like /* @@arg *\/    # minus the backslash  
  
