<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/compilation/DOMParser.php  
  
# class Phad\DomParser  
  
See source code at [/code/compilation/DOMParser.php](/code/compilation/DOMParser.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `protected function get_prop_nodes($item)` Get all property nodes  
- `public function get_item_apis($type, $node)` Get array of apis for this view  
- `public function parse_data_nodes(\DOMNode $item_node, array $on_nodes, array $delete_nodes)` parse `<p-data>` nodes  
- `public function parse_on_nodes(array $on_nodes, \Taeluf\PHTML $doc)` parse `<on>` nodes  
- `public function parse_prop_nodes(\Taeluf\PHTML\Node $item_node, string $item_name)` Parse all prop nodes in an item  
- `public function parse_form_node($form_node, $item_name)` modifies the `<form>` node, sets up `<onsubmit>` and enables `submit` code  
- `public function node_to_php($parent_node, $node_name, $template_param_name, $template_fillers)` Take a node like `<onsubmit><?php echo "cats";?></onsubmit>` and make it so it just shows the inner html  
- `public function parse_independent_access_nodes(\Taeluf\PHTML $doc)` Parse and compile all nodes like `<div access="role:admin">`  
- `public function parse_doc(\Taeluf\PHTML $doc)` Parse & compile a doc  
  
- `public function parse_prop_node(\Taeluf\PHTML\Node $item_node, \Taeluf\PHTML\Node $prop_node, string $itemName)` Parse and compile a prop node  
  
- `public function parse_route_nodes($doc)`   
- `public function parse_sitemap_nodes($doc)`   
- `public function indent(string $str, int $numSpaces, $padFirstLine = true)`   
- `public function setup_error_node($item_node)`   
- `public function replace_node_with_code($node, $code)`   
- `public function template(string $name)`   
  
