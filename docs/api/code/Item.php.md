<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Item.php  
  
# class Phad\Item  
  
See source code at [/code/Item.php](/code/Item.php)  
  
## Constants  
  
## Properties  
- `public string $name;` The name of your item on disk like `Blog/Page` or `Blog/List`  
- `public $mode;` a class const of Phad\Blocks that instructs the compiled item what to return  
- `public string $path;` The base path for an item, like `/path/to/dir/Blog/Page`   
- `public array $args;` Args to pass to your compiled template. Should contain `['phad'=>$phad_instance]`  
- `public string $templateFile;` Path to the template file like `/path/to/dir/Blog/Page.php`  
- `public string $dir;` The base dir for the phad instance that contains all other relative items  
- `public string $source;` This item's template's source code  
- `public $force_compile = false;` true to always compile views (but one instance still only compiles once)  
- `protected $html = false;` Stored output so you can call `html()` multiple times without re-executing  
- `protected $html_with_no_data = false;` Stored output so you can call `html_with_no_data()` multiple times without re-executing  
- `protected array $routes = [];`   
- `protected $item_info = null;` Stored output of your item info, so `info()` can be called multiple times without re-executing  
- `protected $instanceHasBeenCompiled = false;` to prevent the same view object from compiling multiple times  
- `static public array $global_args = [];` args to pass to every item. If item-specific args are set with the same key, they will override the global args.  
  
## Methods   
- `public function __construct(string $name,string $dir,array $args=[])` An interface for your item templates  
  
- `public function rows(): array` Get array of the item's data (instead of html)  
- `public function sitemap_data():array` Get array of sitemap data, used for compiling sitemaps  
- `public function routes():array` Get array of routes  
- `public function info()` Get item info.   
- `public function delete()` Delete the item  
- `public function submit()` Submit data for the item  
- `public function resource_files()` get a 2-dim array of js & css files associated with this view. that is same-named css & js files & css & js files in a same-named subdirectory.  
  
When is say 'same-named' i mean without the `.php` file extension  
  
- `public function html()` Get the item's html view  
  
- `public function html_with_no_data()` Get an html view with no data items  
  
- `protected function source()` Get the item template's source code  
- `public function get_compiled_file_path()` get path to compiled file  
- `public function putCompiledFile($content)` Save content to the compiled file  
- `public function compile()` Compile the item.   
- `public function compileAsNeeded()` Compile only if the template has changed or there is no compiled item output. Will only compile once per instance of this class, regardless of changes on disk.  
  
- `public function create_table_statement(): string` Generate an sql CREATE TABLE statement from this item if it is a form  
  
- `function col_str(string $column_name, array $dom_input)` Generate sql for create an individual column from node properties array  
  
- `public function __toString()` Output html. @see(html())  
  
