<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Controller/PDOSubmitter.php  
  
# class Phad\PDOSubmitter  
INSERTs INTO / UPDATEs database  
See source code at [/code/Controller/PDOSubmitter.php](/code/Controller/PDOSubmitter.php)  
  
## Constants  
  
## Properties  
- `public $pdo;`   
- `public \Tlf\LilDb $lildb;`   
- `protected int $lastInsertId = null;`   
  
## Methods   
- `public function __construct(\PDO $pdo=null)`   
- `public function lastInsertId()`   
- `public function delete(string $table_name, int $id)`   
- `public function submit(string $table_name, array $SubmitData)`   
- `static public function uploadFile($file, $destinationFolder, $validExts = ['jpg', 'png'], $maxMB  15)` Upload a file  
  
