<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Controller/FormValidator.php  
  
# class Phad\FormValidator  
  
See source code at [/code/Controller/FormValidator.php](/code/Controller/FormValidator.php)  
  
## Constants  
  
## Properties  
- `public $throw_submission_error = false;` I don't think this does anything. I really don't know, though.  
- `public bool $allow_extra_fields = false;` Set true to allow extra fields in the submission that are not defined in the form.  
- `public $failedSubmitColumns = [];`   
- `protected $props = [];`   
- `public $validators = [];`  Set `FormValidator->validators['key'] = function($property_value, $property_settings, &$errors)`. Also set `<div validate="key">` to match `->validators['key']`. Default validation will be ignored in this case.  
   
- `protected array $attribute_validators = [];` User-added attribute validators.   
  
array<string attribute_name, callable $callable> key/value array of `attribute_name => $callable`s.  
  
## Methods   
- `public function __construct(array $properties=[])`   
- `public function addAttributeValidator(string $attribute_name, mixed $callable): bool` Add an html attribute validator. Can overwrite built-in attribute validators or user-added attribute validators.  
  
Define `some-attribute="some_spec"` on an html node and call `addAttributeValidator('some-attribute', $callable)`.  
  
- `public function validate(array $data, array $errors=[], array &$failed_columns[]): bool`   
- `public function validatePropTagname($tagName, $propValue)`   
- `public function validatePropRequired(bool $isRequired, $propValue)`   
- `public function validatePropType(string $type, $value)`   
- `public function validatePropMaxlength(int $maxLen, $propValue)`   
- `public function validatePropMinLength(int $minLen, $propValue)`   
- `public function validatePropOptions(array $options, $value)`   
- `public function validatePropPType(string $type, mixed $value): bool` Validate the php data type. Declare `p-type="array"` or another type.  
  
- `public function validatePropPNohtml(string $attribute_value, mixed $user_input_value)` Validate that user input does not contain any html  
  
- `public function validatePropPGreaterthan(string $target_value, mixed $user_input_value)` Validate that user input is numerically greater than the target value. User value MUST be numeric (uses p-type=number validation)  
  
  
