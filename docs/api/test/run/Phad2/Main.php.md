<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Phad2/Main.php  
  
# class Phad\Test\Phad2\Main  
This is a test class for developing some API wrapper classes, to make Phad easier to use  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testWrapper()` Testing a wrapper class that should provide a clean API for working with an item.  
- `public function testEasierPhad()`   
- `public function phad_main(array $options)`   
  
