<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/integration/StatusCodes.php  
  
# class Phad\Test\Integration\StatusCodes  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testThreeStatuses()`   
- `public function testOnlyHavingStatus200()`   
- `public function testDefaultStatus()`   
- `public function testStatusBasedContent()` Uses StatusCodeAccessor to hack the normal access features to explicitly declare access status & access index without need for a database.  
  
- `public function phad($idk=null)`   
  
