<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/integration/Routing.php  
  
# class Phad\Test\Integration\Routing  
Tests that the compiler correctly generates route data & that View class can return it  
  
## Constants  
  
## Properties  
- `protected $template;` the template file contents  
  
## Methods   
- `public function prepare()`   
- `public function testRouteDataFromView2()`   
- `public function testRouteDataFromView()`   
- `public function testRouteDataExtractedFromTemplate()`   
  
