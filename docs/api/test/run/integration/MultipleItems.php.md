<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/integration/MultipleItems.php  
  
# class Phad\Test\Integration\MultipleItems  
Tests involving multiple item nodes in a single view  
  
## Constants  
  
## Properties  
- `public $phad;`   
  
## Methods   
- `protected function standardTest($view,$Blog=null)`   
- `public function testAdjacentItemsWithNestedItems()`   
- `public function testOneNestedItem()`   
- `public function testAdjacentItems()`   
- `public function phad($idk=null)`   
  
