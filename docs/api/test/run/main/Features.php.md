<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/main/Features.php  
  
# class Phad\Test\Main\Features  
Feature tests with as few layers of integration as possible.  
All of these tests should create a compiler, compile from string, and eval the compiled output.  
  
## Constants  
  
## Properties  
- `protected $pdo;`   
- `protected $template;` the template file contents  
  
## Methods   
- `public function prepare()`   
- `public function testSimpleQueryOnAccessNode()`   
- `public function testNodeWithObjectList()`   
- `public function testFormWithSelectOptionSetFromObject()`   
- `public function testFormWithObject()`   
- `public function testXItemWithObject()`   
- `public function testNodeWithObjectAndPropToFilter()`   
- `public function testNodeWithObject()`   
- `protected function itemList($BlogList)`   
  
