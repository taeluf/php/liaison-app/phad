<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/unit/Sitemap.php  
  
# class Phad\Test\Unit\Sitemap  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testQueryToXml()`   
- `public function testFillPattern()`   
- `public function testQueryForSitemapParams()`   
- `public function testNoQuery()`   
- `public function testGetEntries()`   
- `public function testSimpleWriting2()`   
- `public function testSimpleWriting()`   
- `public function testGetResults()`   
  
