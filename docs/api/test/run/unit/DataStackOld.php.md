<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/unit/DataStackOld.php  
  
# class Phad\Test\Unit\DataStackOld  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testNestedItemStack()` In theory, I should test the nested item stack as well, so if I add a 'SongList' nested in a 'BlogList' that is properly represented. However, my one-item-tests kind of ensure this.   
  
After 'row started stack', the head is `$stack->data['BlogList'][0]`, which is an array of blog data. A `SongList` could be seemlessly added to this, the same way a 'BlogList' was added to the root data array.  
  
Since single-item tests already verify that the ending state (after itemListFinished) is identical to the starting state (before itemListStarted), I have high confidence that the state will be restored properly when the nested 'SongList' item is done with itemListFinished().   
  
Similarly, i have high confidence the SongList will be added with the correct structure  
- `public function testAdjacentItemStack()` In theory, I should test for adjacent items, but adjacent item needs the head & stack in an identical state to what it was for the first item. My one item tests already verify that the stack and head have returned to their initial state.  
- `public function testOneItemStack()`   
- `public function testOneItemHead()`   
  
