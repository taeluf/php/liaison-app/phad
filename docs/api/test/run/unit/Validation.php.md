<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/unit/Validation.php  
  
# class Phad\Test\Unit\Validation  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testAllowsEmptyIfNotRequired()`   
- `public function testWithValidator()`   
- `public function testMinlength()`   
- `public function testMaxlength()` test maxlength property validation  
- `public function testExample()`   
- `public function testValidation()`   
- `public function testAttributeValidation()`   
  
