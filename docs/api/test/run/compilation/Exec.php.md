<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/compilation/Exec.php  
  
# class Phad\Test\Compilation\Exec  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function run_exec_test($view, $args, $expect, $handlers=[])` compiles the view, passes args to it, and compares against expected html  
- `public function testCanReadNode2()`   
- `public function testFormWithErrorItem()`   
- `public function testInnerLoop()`   
- `public function testBlogListArg()`   
- `public function testBlogArg()`   
  
