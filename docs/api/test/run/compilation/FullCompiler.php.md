<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/compilation/FullCompiler.php  
  
# class Phad\Test\Compilation\FullCompiler  
Tests to verify compiler output  
  
Tests I may need to add in the future:  
- multiple <p-data> nodes  
- multiple <on> nodes  
- <on> nodes nested inside <p-data> nodes  
- item nodes nested inside other item nodes  
- item nodes adjacent to other item nodes  
- multiple routes  
- general flow of logic (probably won't ever go in THIS one though)  
  
## Constants  
  
## Properties  
- `protected $template;` the template file contents  
  
## Methods   
- `public function prepare()`   
- `public function testBackendInputNodesRemoved()`   
- `public function testRouteNodeRemoved()`   
- `public function testIncludesOnSubmitWhenThereIsANestedItem()`   
- `public function testCanReadNode()`   
- `public function testAddFormNodeInfoWithFileInput()`   
- `public function testAddFormNodeInfo()`   
- `public function testOnS404()`   
- `public function testOnS200()`   
- `public function testAccess()`   
- `public function testFormOnSubmit()`   
- `public function testFormProperties()`   
- `public function testFormTarget()`   
- `public function testXItem()`   
- `public function testSitemap()`   
- `public function testSimpleRoute()`   
- `public function testItemCanReadRow()`   
- `public function testItem()`   
- `public function testOutput()`   
  
