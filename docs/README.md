<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Phad: Php Html Api Database  
A templating system that compiles HTML into database-connected PHP.  
  
**NOTICE:** While Phad is feature-rich, robust, and used in several production websites, it isn't ready for mainstream use. Some of the codebase is very poorly designed, integration is unnecessarily complicated, documentation is insufficient, and it's just much harder to use than it should be.  
  
We plan on fixing these problems, but have no ETA on when this will be. Our branch `v1.0` is an attempt to fix these problems, and we are making progress on it. v0.5, v0.6, and v0.7 were also attempts to fix these problems, but they were overly-ambitious rewrites. v1.0 hopes to bring minimal breaking changes while significantly improving the internals, with a fair bit of re-structuring, but minimal rewriting.   
  
## Install  
```bash  
composer require taeluf/phad v0.4.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/phad": "v0.4.x-dev"}}  
```  
  
  
## Documentation  
- [Basic usage](/docs/Usage.md) - Examples of various views & initial setup  
- [Cheatsheet](/docs/Cheatsheet.md) - Quick overview of functions & features, plus   
- [Extended Examples](/docs/Examples.md) - Examples of sitemap generation, access controls, spam controls, handling errors, and more.  
- [Troubleshooting](/docs/Troubleshooting.md) - Known issues & solutions  
- Utilities/Features  
    - [Form Validations](/docs/FormValidation.md) - Write forms, validate them automatically on submission, or validate data manually  
- [Architecture](/docs/Architecture.md) - An overview of the architecture, for development of Phad.  
    - [All Classes](docs/AllClasses.md) - A list of all classes in this repo  
    - [API Documentation](/docs/api/) - Full documentation of each class.  
    - [Old Architecture Documentation](/docs/Architecture2.md) - Probably better documentation than the above linked Architecture docs.  
  
  
## Example View  
When `/blog/some-slug/` is requested, `SELECT * from blog AS Blog WHERE Blog.slug LIKE 'some-slug'` will be executed (*using property paramater binding via PDO*). Then the `h1` will be filled in with `blog.title` and `<x-prop ...>` will be entirely replaced with `blog.body` after it is converted from Markdown to HTML.  
  
```php  
<route pattern="/blog/{slug}/"></route>    
<div item="Blog" >    
    <p-data where="Blog.slug LIKE :slug"></p-data>    
    <h1 prop="title"></h1>    
    <x-prop prop="body" filter="commonmark:markdownToHtml"></x-prop>    
</div>    
```  
  
Sitemap information, PHP code, and access controls (like `role:admin`) are also available, among several other features.  
  
## Example Form  
`GET /form/blog/` will display this form. If `?id=37` is passed, then the blog post with `id=37` will automatically be filled into the form.  
  
`POST /form/blog/` will save the blog post to database, whether `INSERT`ing a new article, or `UPDATE`ing an existing one.  
```php  
<route pattern="/form/blog/"></route>    
<form item="blog" target="/blog/{slug}/">    
    <onsubmit><?php    
        $articleRow['slug'] = \YourNamespace\slugify($articleRow['title']);  
    ?></onsubmit>    
  
    <input type="text" name="title" maxlength="75" />    
    <textarea name="body" maxlength="1500"></textarea>  
  
    <input type="submit" value="Submit" />  
    
    <input type="hidden" name="id" />    
      
    <!-- The `backend` type allows the form validation to permit slug being added during `onsubmit` ->  
    <input type="backend" name="slug" />  
</form>    
```  
  
Forms also support `<didsubmit>`, access settings (like `role:admin`), file uploads (*kind of, its messy*), and more features.  
