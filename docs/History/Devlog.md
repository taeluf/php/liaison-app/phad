<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Phad Devlog  
Intended to be a historical log of notes & stuff, just in case I need to return to this stuff later  
  
**Note (feb 4, 2022):** I have reviewed all the todos/known issues at the bottom of the file & they should not be reviewed again. I reviewed all the recent notes (I think Jan 26 to Feb 3) and those should not be reviewed again. All notes i care about have been removed. Though the "Testing Notes" from jan 26 still may be of use & may need further review  
  
## Testing Notes (jan 26, 2022)   
**NOTE:(feb 4, 2022)** these notes should be put into chronoligical order after i am done with them, but if not ... whatever  
  
  
 I'm trying to figure out how to write a good phad test ...  
 one that tests the final output from executing a compiled view  
 certain inputs will need to change (what inputs)  
 and verify against different outputs  
  
  
item template yields compiled output... this step does not change, other than the the view input  
  
  
  
 what inputs generally stay the same?  
     args in general (unless they change in userland)  
     phad object  
  
  
 what inputs change?  
     view name (and potentially directory)  
     the mode (html, sitemap, submit, delete, data, item info, routes)  
     the user (and their ability to access things)  
     the data source ($Blog v $BlogList v running queries)  
  
intermediates that are important  
     $ItemInfo->response_code  
  
  
 what gets called upon `$phad` from the item's compile source?  
     $phad->delete($ItemInfo)  
     $phad->can_read_data($_node, @$$ItemInfo)   
     $phad->read_data($_node, @$$ItemInfo);  
     $phad->object_from_row(@$$Item_Row, @$$ItemInfo)  
     $phad->user_has_role('admin'), but it is a handler, not implemented directly on Phad  
     $phad->submit(@$$ItemInfo, @$$Item_Row)  
     $phad->can_read_row(@$$Item_Row, @$$ItemInfo, @$$ItemName)  
      AND, maybe  
         $phad->get_rows($ItemInfo) ... but it shouldn't be there. It just needs to return @$$ItemInfo->rows bc `read_data` is already done prior to this part ... unless I want it to have that extra layer for filtration (in case of phad being overridden)  
     AND, maybe  
         I might add a `$phad->start_item(@$$ItemInfo)` ... just because it's a nice hook ... but i should leave that off until I actually need it  
  
 what handlers does `$phad` have? (handlers are set explicitly on phad & are invoked via `__call()` implementation, so a subclass can just define the method)  
     $phad->user_has_role($arg);  
  
  
 what about the item needs testing? (these are basically the actual features of phad)  
     get an html view  
     get an item's meta info  
     get an item's routes info  
     get an item's sitemap info  
     delete an item  
     submit an item for insert   
     submit an item for update  
     file uploads  
     show an html view from passed in Item  
     show an html view from passed in ItemList  
     show an html view by querying on a data node  
     loading different item data by controlling which data node is accessible  
     rejecting access to data as a whole  
     reject access to specific rows  
     responses based upon status code 200 v 404 v 403 v 500  
     property nodes being filled in  
     TODO? filters on property nodes  
     forms being filled out properly  
     TODO redirection on form submission  
     DONE showing nested items  
     DONE showing adjacent items  
  
 what else needs testing?  
     an individual node's `access="admin"` feature  
     built-in error reporting (to the dev & the user) for certain things (failed form submission, at least)  
  
  
## Feb 3, 2022 END OF DAY  
I did a bunch of stuff today ... normalized calling of access handlers (`call:whatever` in an attribute) ... idk. idc  
  
Since tomorrow is friday, i think ... i think maybe i should spend the day cleaning up this repo ... just deleting stuff i don't need any more ... moving my notes to devlog ... idk what else  
  
  
  
## Feb 3, 2022  
- i need to stop forms from displaying multiple items when there is no id given ... it just needs to display an empty form then ... except i don't know how to recreate that error ... so for now, i guess i just ignore it  
- need to write down the `<errors></errors>` feature ... maybe make it customizable? meh  
- need to genericize the 'candelete', 'cansubmit' and 'diddelete' in the `DOMParser`  
  
## Feb 2, 2022 END OF DAY  
I'm stopping before working on `pt DeleteRedirect` ... uhh .. the routes trait is trying to return a response, but i think i changed it so it does an auto header() & exit on submit & i feel like it should do the same on delete ... but idk ... i might need to switch back to the get_response() approach & ... ugh idk idc. I figure it out  
  
bears are cute  
  
## TODO  
Next is `pt DenyCustomAccess` ... the problem here, far as i can tell, is that i just haven't re-implemented the 'custom_access' thing or the 'permit_me' ... this still might require some change to phad ... but ... that should be relatively easy? And then i just need the user-land function to handle ... whatever handler idk....  
  
- write this down somewhere: `access="role:admin;call:whatever"` ... to check role AND call a custom handler   
  
## `<on s=###` Nodes (feb 2, 2022 AFTER changes)  
- If a data node gives success, then ONLY that node's `200` status will be shown  
- if no data nodes give success, then every data node will show it's status for whichever status that particular node ended with  
  
## `<on s=###>` Nodes (feb 2, 2022)  
My whole frame of thinking for status nodes was wrong ... if i have multiple data nodes & each one has its own status nodes, and there are three data nodes which all fail, which status nodes do i display? All of them? That just doesn't seem right? So when do i decide THIS is the data node for which status nodes should show? How do I make that decision? Is there some kind of flag i can put on data nodes to say "come back to this one on failure"?  
  
And I'm using the data index to determine which set of status nodes to dispaly ... but that dat aindex is only set on success ... so i'm trying to think ... if i check three nodes & they all fail, then is my index the last one that failed? Well i had it in my mind that a SUCCESS was the only thing that sets the index ... so this thinking is flawed  
  
I suppose it is within reason to show status nodes from EVERY data node that was attempted, whether it's the last one attempted or not. That puts the onus on the developer USING phad rather than me on the developer of phad ... that is generally my preference ... i don't think phad should make a lot of decisions ...  
  
hence the way i implemented `access="role:admin"` ... it doesn't auto-split args or anything ... that's also partially just the dev time required to do something like that .... idk ugh   
  
  
so I'm thinking about disregarding the data index & displaying status nodes for every data node whether it succeeded or failed ... in this case, we still stop checking data nodes once we have a success  
  
  
okay, so   
  
## Feb 2, 2022  
I am accumulating technical debt in the form of the query + access setup ... it just ... it just is awful. I need to figure out a way to properly test the access stuff without it being horribly cumbersome. Because there are many branching possibilities ... Also ... the query + access code just really needs cleaned up & the whole way the `default` access is handled is ... concerning. There is logic to do with the default data node in ... well, i think just two places? But it seems to just keep getting more complex & messy ... this would make me vulnerable to an exploit.  
  
## Feb 1, 2022 END OF DAY  
So all tests are passing, except for the Server\Main tests ... i haven't even started on those ...   
I had to do stuff to make delete() work & add can_delete() ... i had to include ROUTE_META & SITEMAP_META in every single compiled output file, even though I don't want to ... otherwise, i don't know how to systematically process every view for route/sitemap info ... since the default is to just display ... ic ould have maybe wrapepd view output in a `$phad_block==Blocks::VIEW`, but then that requires extra stuff from `submit()` when it fails (and maybe delete?). ... I don't like my solution, but I don't want to do any major refactors  
  
Also new known issues: Status codes stuff ... (see Known Issues in this file) ... and there's whitespace after the SITEMAP_META block ... which should be okay bc item wraps everything in an output buffer ... but i could see it causing a problem at some point. I think it might be worth fixing ... but idk. ... i'm just so tired of this ... i just want to USE phad, not work on it any more.  
  
I don't know how the server is going to work now ... probably not that much different from before ... I think the route function will need some changing & maybe I'll re-implement Phad's `main()` function ... i think i'd like to do that, so it's just ... a little bit easier to use.  
  
So once i get the server tests passing ...  
then i think it's time to clean up this status doc, at least move my dated notes into devlog.md  
  
There's plenty repo cleanup i could do. The tests are poorly organized ... The primary code itself is actually fairly nice, though some of the decisions about where methods go (Phad vs Routes trait vs Submission trait) are a little iffy ... but that doesn't really matter.  
  
It would be nice to document some of the things ... but i think a lot of stuff is already documented well via `@tags` but ... code scrawl doesn't really support what i need yet in order to find & display those docs ... and I don't think it's worth my time right now ... i think i have other priorities.  
  
But I should probably start a file to document common things ... and then when i ... OH, i have an `FAQ.md` file ... maybe i should review that ... then just add stuff to that as-needed  
  
  
## Jan 31, 2022 END OF DAY  
I stopped on test `ErrorMessage` in class `Integration\Forms` ... i'm still working through tests from bottom to top. I have not started the `Server\Main` tests ... that will come after all the other tests  
  
The issues is getting the list of errors ... i'm not sure how i'm gonna do that ... ugh idk. my brain is mush at this point. I'm exhausted of working through these tests. I'll figure it out.  
  
Umm. Good morning, me. Hope you slept well. Hope you're in a good headspace. Remember to be calm & patient. Remember to make mental space. Remember to ... remember instead of just bouncing around. Just ... try ot enjoy yourself.   
Love,  
Me  
  
<3 hehe  
  
  
  
## Jan 31, 2022  
- @todo(eventual) use `accepts="image/png"` to limit the files that can be uploaded  
- @todo(eventual) automatically setup `file` inputs with the appropriate backend-properties (rn its totally manual)  
  
## Jan 28, 2022 end of day  
I stopped on `phptest -test OnS200` ... the problem is that I never implemented `<on>` nodes in the new compiler outside of data nodes ... i think i want to implement this ...   
  
After that, I'm just working my way up the failed tests bottom to top.  
  
I just removed all the old code (well, i put it in an `old` folder) ... and i've just been making minor changes to tests & to my new code to accomodate the little things i'm missing.  
  
I may want to look at Integration tests before `Server` tests ... but we'll see.  
  
Have a good day! :)  
  
## Jan 28, 2022  
i finished the submit stuff & the error stuff & the exec test. There's quite a list of things in a `TODO.php` file in the test dir ... but idc ... like, a lot of those things are covered by the existing tests (whether it's organized well or not) ...   
  
So ... I'm gonna delete the old code, make a bunch of tests fail, and get to work making it all work again  
  
## Jan 27, 2022 end of day   
I'm working on the `Phad\Submit` test class ... I added errors to submission (and modified formvalidator to return failed columns). I added inner loop for items. I added special `$arg` handling to make the submission errors easier (see the `Exec` test class for a full example)  
  
Umm. I'm not sure about next steps. I'm too out of it at this point. Read notes? Think about it. Figrue out next steps. You got this.  
  
## Jan 27, 2022  
- i was working on submission tests ... then wanted errors ... so started a test with an item nested in a form & that item would load from an automatic 'Errors' entry in `$args` ... but the nested item isn't processed at all ... so now i need to test nested items  
  
so now to make the errors work nicely, I'm adding `loop="inner"` to loop on the inside of an item node, rather than the outside. so now i'm testing that  
  
## Jan 26, 2022 end of day  
i renamed a bunch of stuff. cleaned up some code. moved files around. I mused about how to start the transition to the new compiler, but i was just stuck ... imagining I'd just ... break EVERYTHING in the process & have much stress. So I started working on a new test class. See `test/run/full/Phad.php` where I have notes ... I'm trying to think of a clean way to write these tests ... bc there are many different inputs/branches & features to check through  
  
There's a long list of things there to test ... what I'm TRYING to do is figure out how to make this easy to test & how to make the tests easy to understand. I will likely be writing some kind of main test function that takes in a fairly simple set of data & allows me to easily run through different branching possibilities.  
  
For example, just specifying `user_role="admin"` when calling this test function would make it really easy to test granting access to different data nodes  
  
Definitely see that test file & go from there ... let's try not to overdo or overthink things. I want to get this changeover done, so i can get back to product development  
  
## Renaming  
These changes are in both the `code/` dir and `test/` dir  
- Rename `View` class to `Item`  
- replace `$view` with `$item` (also affects `$view_name` & other likeminded vars)  
- replace `view(` with `item(`  
- replace `views(` with `items(`  
- replace `view_dir` with `item_dir`  
- replace `item->data()` with `item->info()`  
- rename `item->output()` to `item->html()`  
- rename `item->outputWithEmptyObject()` to `html_with_no_data()`  
- did some other renaming, too ...  
  
  
## TODOs  
prior to changes, ALL prototype tests are passing, except the one `TemplateIdk` test, which is disabled.  
- in parser & compiler  
    - review method names & argument names  
    - review functions for more consistent variable names  
    - add docblocks  
- in tests  
    - update to new method calls  
    - change argument and var names   
  
... "compiled" output vs "parsed" output ...   
- DomParser??  
    - DomParser output   
- TemplateCompiler??  
    - TemplateCompiler output  
    - template args is the list of paramaters found in a template  
    - template fillers are the arguments that fill in a template  
  
  
## Jan 24, 2022 END OF DAY  
see `TemplateCompilation.php` test  
...  
So, i ran into an issue when impementing `<div access="role:baby">` bc the current implementation of parser, compiler, and my tests does not care about the phtml doc ... it only cares about the output from the parser ... and the parser only outputs anything for `item` nodes ... ... it DOES modify that `<div access=...>` ... but it modifies it in the doc and does NOT return any code  
  
So, i need to start inserting things back into the doc ... i feel like this deserves another class ... i don't think it should be the compiler or the parser ... but maybe the parser can do it? Meh ...  
  
the Parser is responsible for parsing nodes, modifying them, and returning template args to use with the compiler. The compiler is responsible for loading a template and filling it from an array of arguments. Then something else is (should be) responsible for managing this process so that I can output a full `$doc` that has been parsed & compiled ...  
  
I might want to rethink some terminology ... But Idk ... I don't think it's necessary ... some places it is confusing, like `ItemInfo`??? that's just .... idk .. idon't like it ... Parser's get_template_args() should probably be get_template_args()  
  
I do want to think in terms of template args (which are passed to the compiler to fill in a template) .... and it would be nice to change variables & function names to accomodate that ... it is easier to think about that way ...  
  
...  
...  
today, i added form parsing & added form compilation ... i cleaned up teh `on404_403` test (and the `on404` test). I added a test function to execute blocks of code returned from the parser (so i can test parsed code in chunks, rather than having to do it in the full compiled output)  
  
...  
  
Parser's `parse_form_node` method is yucky ... it's actually pretty easy to follow, but it is 60 lines & I don't love that ... so i might want to chunk it into smaller functions  
  
Also, with parser, I might want to re-evaluate the names of my functions ...  
  
...  
  
I'm well aware the compiler is pretty messy ... i don't think i care ... it does a pretty simple thing & it works & i have unit tests so ... it just doesn't matter much  
  
...   
  
I should probably docblock the parser too ... its really nice to have descriptions when i come back later & don't remember how stuff works  
  
... there's also some template args that i could change to a more normalized style ... like `form_submit` could become `has_form_submit` or something so it is more descriptive. `can_read_row` could become `has_can_read_row` ... idk ... i have noe that is `form_is_deleteable` and i really like that. `is_form_deleteable` would be fine too  
  
....  
  
I COULD create a template for the `<div access="role:doggie">` ... but the compiled output code is so simple i don't think it's worth the effort  
  
...   
  
also ... i keep calling the modifications from Parser "compiled output" and i don't like that because it is distinct from Compiler's output which is the final compilation ... but calling it `parsed output` sounds weird? Idk.  
  
  
...  
  
I'm getting very close to cleaning up the prototype classes, making a new phad version, and replacing the old phad code with the new prototypes.  
  
That sounds overwhelming and I'll need to be thoughtful about my approach  
  
...  
  
thanks for doing the long read, for typing the long notes, and for continuing to write cool ass code.  
  
To Reed, from Reed: Hope you're happy today :)  
  
---  
  
  
## Jan 24, 2022  
the parser is refactored  
the parser tests are refactored (and pretty dope)  
I'm stil missing features (forms, `<on>` nodes that are NOT descendants of data nodes)  
  
  
## Jan 21 late night / jan 22 23 mins after midnight  
statuses are working now ... I have a compiler test written, but it's in the Parser test class. This test needs cleaned up & put it in its appropriate place & parser needs a test too & exec needs a test to confirm this feature ... ugh  
  
The Parser code is an absolute mess ... it needs majorly refactored ... this should be the next step, then cleaning up the new tests in Parser (either order should be fine)  
  
I refactored the parser code  
  
  
## January 21, 2022 end of day  
  
technically the "late night code" from yesterday's notes was mostly THIS MORNING. wild. I know  
  
So, I made a lot of progress on the parsing & compilation. The template is working & being filled completely. I wrote unit tests. I wrote helper functions to run unit tests so that my unit tests may be incredibly declarative rather than requiring nearly identical logic across a bunch of different tests. I should probably better notate things, though ... and keep one test around that DOES implement a bunch of its own logic, so I have a good example of it all being together .... idk  
  
What's next?  
Continuing work on the parser. I did `<p-data>` nodes (`<data>` is an existing html element ... it is tempting to use it anyway). I added `Exec` tests that compile & `eval()` code and verify output. I added `Parser` tests which get an array of items (rather it's values to fill in placeholders in the template file (so maybe i should rename some things)) & verify the item's are correct. I added `Phad2` tests that test phad_methods like `read_data()` and `can_read_data()`. There are already `Compiler2` tests that have nothing to do with Phad & just verify that code is output correctly (i could update those to use `compare_lines()`).   
  
So what is next?  
More parsing ... also need to fill out the template for form stuff ... need to build access ... just look at the template, look at a form, and look at some other views & then figure it out at the desk when you're fresh.   
  
Okay, bye. Have a good weekend, Me! :)   
  
(and hope you HAD a good weekend, when i get back)  
  
## January 20, 2022 end of day  
I worked on a new compiler prototype that uses a template to compile php code ... which makes things a lot cleaner & easier. in `code/prototype` there's a compiler2.php & template.php with a super simple implementation for compilation & then a pretty thorough template that is probably very close to what I actually want my template to be. Though a few things are not yet included on it. I made notes about those in the template file.  
  
This is probably not a priority ... but the new templating is very exciting. If i get past prototyping & start replacing the old compiler, then I need a new branch  
  
**LATE NIGHT UPDATE:** the php code compiler is probably done. Now it's just a matter of converting my old compiler to a scanner that extracts whatever i need for filling the template. the prototype compiler is messy, but it has tests & it works. If I ever want to add features, it'll kind of suck ... but I also wrote it in ... 3 hours, maybe 4. So it doesn't really matter. If i have another go at it, I'll use a different structure & probably make it easier ... idk.   
  
I might want to refactor the code I have. It is fairly messy. The test code is pretty messy too.   
  
  
**END OF LATE NIGHT CODE:** In `phptest -test Parser` shows taht I am successfully inserting html code into the template. I had to fix an error with my sorting function (I was treating $a and $b as values, but they were keys, because it was `uksort()`) ... all the not-enabled code is removed. There's a lot of extra whitespace in the file, but that's inside php code blocks, so idc.  
  
I need to write unit tests that use the new compiler for various different outputs from my template ... then write the parsing of nodes so that I can insert the nodes into my templates ...  
  
I need to finish filling out a few features in the template, too. See template.php.bak (it has notes I wrote down, but then deleted from the current version for simplicity of review)  
  
## January 17, 2022  
Basically, I'm just gonna work on the vote site. The notes here will still need to be sorted.  
  
### Added  
- (jan 19, 2022) `<access where="Meeting.office_id = :get.office_id">` to auto-use `$_GET['office_id']`  
- (jan 19, 2022) phone & email validation  
- (jan 20, 2022) a prototype for a new compiler that uses templates to insert code as-needed  
  
## Jan 14, end of day  
I did route caching, then hecked around doing benchmarks that don't matter. I modified the routing to NOT use an anonymous function any more.  I probably did other things today before that ... oh yeah. some tests  
  
I also did individual node access.  
  
So ... I think I decided that the rest of the "essential" features are not actually essential. So I think Monday, I'm going to try to update my Vote website to use Phad & the latest Liaison.   
  
So ... I think I'm done developing Phad for awhile, except for small features & fixes as I need them.   
  
Before I put it down, I need to review the notes here & organize them. When I come back around to implement APIs or more caching or whatever ... I don't want to figure out again things that I already figured out.  
  
So ... yeah.  
  
Hope you had a good weekend, Reed. Welcome back! <3 :)  (yes I leave nice notes for myself cuz i silly)  
  
  
## Jan 14  
I'm kind of sick of working on Phad. It has most the features I need it to & the features it doesn't have ... well ... I'm not sure how much I need them or if I'll even like my implementation. I also really want to refactor some access stuff, but ... meh ... I just need to write good notes about the things I'm NOT doing & finish the caching & individual node access.  
- API: I need to write out my plans for how this will work, but I don't need to implement right now. I think this will be a really important feature for convenience, but I don't think I really need it & the workarounds are simple  
- Modal: This isn't really Phad's responsibility. I want a nice modal thing ... but it's just not what I need right now.  
- Two stage deletion ... I could make a simple route that essentially takes a delete request & re-packages it as a form that's displayed in a modal & a confirmation click will then ACTUALLY do the deletion. This isn't really Phad's responsibility.  
- Pagination: I'll work on that when I come to need it.  
- Caching: Routes, yes. I think that's it.  
- Individaul node access: yes. Very much in Phad's purview.  
  
  
## Jan 13, 2022 end of day  
I did some tests today, mainly regarding `deleteable`. I noted some things in `FAQ.md` about deleteable. I added custom access handlers (`<access handler="handler_name">`).  
  
I also did a little work in `lildb`, my test lib, and my bash tlf lib. lildb work was necessary to make deletion work.  
  
Overall, I think it was a pretty good day. I got some things done. There's a couple more tests for me to do, but the next thing to really work on is the planning of caching, apis, modals, javascript, and individual node access.  
  
I thought about this stuff last night & thought I would write my notes below, but ... it kind of fell apart as I was trying to explain.  
  
I will need to compile all the views & get a list of them all & store that list on disk ... possibly caching some of the view information that would normally be retrieved by `get_item_data` phad mode or something. In this same vein I need to get all the routes from all the views.  
  
I also want to implement the router not as a router that handles a particular pattern, but as a catch-all router that just returns a route if it is successful & defers to the main router if it does not find a route. This would address a lot of the complexity, I think? And allow me to add smoe efficiency ... but this also might not be necessary.   
  
When I'm implementing the caching, there's really two different things to test & do ... 1. get the list of all the [whatever thing] and 2. cache the list of the [whatever thing] ... the getting & the caching are distinctly different steps & i want my inital test to be about the getting, NOT the caching. Then once the getting is reliable, I can do the caching in whatever way seems appropriate.  
  
I also need a separate build function that runs through all this caching & I need to be able to test that build function. THEN I can OPTIONALLy cache the build-process ... perhaps a flag file that if the file DOES NOT EXIST, then the build will be run & if the file DOES exist, then DO NOT BUILD. I could add a cli interface but meh. I could make a REALLY straightforward cli script that just deletes the file & calls the cache-build function or something  
  
every view should have a list of apis that it creates. a standard view can have an `html` api and a `json` api. A form can have a `create`, `update`, and `delete` api ... but maybe `create` and `update` could be the same... at least for now  
  
Then with that list of apis, I can make an api-router that takes a string like `Blog:create` and maps it to the appropriate form. Simple version of the API could be like `/phad-api/?item=Blog&action=create` or just `?api=Blog:create` ... though it WOULD be nice to have a cleaner url like `/Blog/create/` or `/Blog:create/` ... idk ... it doesn't really matter ... regular users won't give a shit & I won't know what developers want until I have feedback (including from myself)  
  
A big part of the caching is to enable features like the api ... I really can't do api routing without already loading ALL of the views ... and running through ALL of the views on a single request is just ... so wasteful. So silly.  
  
But routes can also be enhanced by this. Right now, routes require finding all of the files, `require`ing them, and adding the route to liaison. I'd rather have all of this route data already pre-compiled in one file. What I would REALLY like is for the routes themselves to simply BE file paths ... where the file path would do some necessary setup & then load the compiled php phad file.  
  
I'm still ... SORTA on the fence about access to individual nodes ... I definitely like the idea, but i don't want to implement it .... ugh ... but i should. I should because it's an extremely likely use case ... say somebody makes a comment ... that user can edit the comment (access) or an admin can edit the comment (access), but any random user cannot. Also, an admin could delete the comment whereas anyone could report it. So it would be really nice to make this easy nad have something like:  
```  
<a href="/add-comment/" role="*">Add Comment</a>  
<a href="/delete-comment/" role="owner|admin">Delete Comment</a>  
<a href="/edit-comment/" handler="can_edit_comment">Edit Comment</a>  
```  
  
but it might be better to do `<a access="role:*">` or `access="handler:can_edit_comment"` or something like that ... yeah ... bc then I add one access to it & blah blah blah ... don't have to handle a bunch of different new tags ...  
  
but then do I treat it like an item node? NO because there IS NO DATA  
  
  
Okay ....  
So I also would LOVE to change SOME of the access around bc there are distinct stages of access and having names like `hasAccess` is ... dum & confusing  
so ...  
- `has_query_access` or `can_exec_query`  
- `has_row_access` or `can_read_row`  
- `has_delete_access` or `can_delete`  
- `has_create_access` or `can_create`  
- `has_update_access` or `can_update`  
- `has_node_access`  or `can_read_node` ... for the independent nodes (or `can_view_node`)  
  
adding more deliniation to the access to make it clear what is being granted ... would be really nice ... would also be easier to implement custom access in ONE area without disrupting another.  
  
The reason I DONT want to do this is ... it's work & time ... I feel like there may be better things to do with my time ... but also this seems like it might be fairly important? Idk ... It can probably wait ...  
  
...  
  
also there's smoething confusing about the access/load item/ delete flow. since checking access also runs the item loading, deletion has a `404` before a `DELETE FROM` query is run ... I handled it. It is fine. It's just a little strange.  
  
## Planning... jan 13, 2022  
- caching  
    - All views as view_name=>top level item  
    - all views as top level item => view_name  
    - `items=>['views'=>['view/1','/view/2',etc], 'apis'=>'create'=>'view/1']`  
    - routes  
- api like `Blog:create`, `Blog:html`, `Blog:delete`, `Blog:json`  
- modal  
    - just some simple html + css that the response gets put into  
- javascript  
    - submit form via js (from modal, or from its own page, or whatever)  
- access on an individual node  
    - `<a href="/whatever/" role="admin">New Blog Post</button>`   
- api on a node  
    - `<button api="Blog:create">New Blog Post</button>` auto-add an on-click listener to request the api url  
    - `<a api="Blog:create">` auto-set the href to the api url  
  
## Roundabout jan 12 ... ish  
  
## Non-Essential TODO  
- `default="some text or whatever"` to have a default value when nothing is sent... This might work for displaying things too, where the db item doesn't have the content, so the default is used instead  
- For Form submissions. Ex: Form has `<input type="text" prop="title">` & wants slug. Add a new node `<input type="computed" from="title" filter="slugify" to="slug">`  ... intent is to use pure-html to filter the input title into a slug (so the `slugify` filter just has to be written once)  
- `Sitemap`  
    - `<sitemap name="BlogPosts">` to explicitly name the sitemap file & route.  
    - handlers to modify sitemap data on a row-by-row basis  
    - Output multiple sitemap files & an index sitemap file listing all of them  
- `<access>`  
    - MAYBE (see devlog, toward bottom) `<accessgroup>` to require multiple accessors get passed  
    - `<access log_error="always">` to always log the error (or something better)  
    - `<access throw_error="always">` to always throw the error  
    - `<access continue="true">` <- If this access is reached, it must pass in order to try the following accesses.  
- address known issues  
- Listing columns explicitly in `SELECT` statements  
    - implement by scanning item data for `properties`  
    - Add `columns` attribute to include more properties  
        - OR add a special node that disappears when compiled, but adds the property to the `properties` array  
        - Are these columns publicly available? (on a JSON request, for example)  
  
## Refactors TODO  
- `Filter`  
    - add a proper api for adding filters (currently just uses direct access to public filters prop)  
- use constants for different phad modes (or an enum)  
- rename `<access>` to `<data>`  
- rename `ItemData` to `ItemInfo` or `ItemInfo`  
    - maybe make a class for item info  
- `Compiler`   
    - hooks for modifying nodes, such as adding schema.org attributes  
    - use modtime to determine if a view needs to be re-compiled. When compilation is done, set the modtime to be the same on both source template & compiled output. See php `touch` function.  
- responding to routes/requests  
- setting up liaison routes  
- the logic-flow inside a view  
    - separate access-checking from item-loading  
    - maybe use a class with methods to handle different phad_modes  
- `View` ... does too much, I think ... and `View` probably isn't the right name  
  
## Latest  
- DONE caching routes & unnecessary benchmarks of the different routings  
- DONE individual node access + can_read_node & user_has_role functions in access.  
- DONE (several things I didn't write down here)  
- DONE implement & test `deleteable` response ... do like `deleteable=command:arg` or `goto:/target/url` or `print:some message`  
- DONE(jan 12, 2022) `Integration`s class ... just very simply encapsulates integrations, like adding routes to liaison or setting up the commonmark filter (recommended, but not required dependency)  
  
  
## Jan 12 & jan 7  
## Tests TODO   
- DONE try to delete an item where `deleteable` is not set (deletion should fail)  
- DONE deny access to deletion while allowing access to creation  
- response for:  
    - DONE failed submission  
    - NOT DOING failed file upload   
    - DONE failed item deletion  
- (meh) unit for `SitemapBuilder::make_sitemap()`  
- (meh) unit for `SitemapBuilder::get_sitemap_list()`  
- (meh) unit for `Phad::create_sitemap_file()`  
  
## MVP Features TODO (jan 12)  
- api, modal stuff, javascript stuff  
    - `<button api="Blog:create" role="admin">New Blog Post</button>`   
    - javascript-based form submission  
    - `<a href="/admin/" role="admin">Admin page</a>`  
    - A way to explicitly call an api via item-name+api method, like `get(/phad-api/?item=Blog&method=delete)`  
  
## Performance TODO  
- caching   
    - all views  
    - all routes  
    - sitemap results per-view  
    - item info for each view??  
  
## Essential non-urgent TODOs  
- pagination to handle showing MANY items  
  
## Jan 7, 2022 END OF DAY  
I just cleaned up a lot of old notes & code. I choose not to clean up TestHelper and Tester ... though it would be nice. I know I need to refactor. This is a big endeavor. I wrote a lot of notes below that probably cover moooost the things that still need to be done.  
  
I'm quite sure ... the next thing I need to do is refactor all my routing. I want a Liaison-independent implementation for routing & I want an `Integration` class that makes the routing work with Liaison. That `Integration` class should also setup commonmark as a filter (optionally). Unless it starts to feel bloated, it should just be SUPER straightforward and have individual methods to setup integrations with different external pieces of software.  
  
I also want to refactor the compiler, `Access` class, and the compiled output ... These feel urgent, but I don't think they are. I think they only feel so necessary right now because I just added `deleteable` and that was challenging.  
  
Oh, on `deleteable` ... I implemented it, but only tested a single success case. Also need to test TRYING (and failing) to delete when the `deleteable` attribute is not listed on the form item node. Also need some kind of redirecting or success response or SOMETHING  
  
I ALMOST like the idea of wrapping the compiled template in a controller class, such that it calls `$this->whatever()` instead of `$phad->whatever()` ... I might actually like it ... but I don't know ... It's ... less testable that way, maybe? I don't know. This is all complicated and messy & I'm trying to make it simple and not-messy. We'll get there.  
  
## Why was it so hard to set up `deleteable`? & how can that be fixed?  
I didn't know where to put the logic for it. I didn't know where to do the actual deleting ... I didn't know where/how to do the routing ... I didn't know where/how to make it work well with the current access implementation ... my brain was not in big think mode & I was not able to understand the broad picture ... So why couldn't I understand/know those things?  
  
... View is completely unnecessary. It just makes it a little bit easier to execute different tasks in a compiled template. It is USEFUL, but it doesn't DO that much & the current implementation, I think ... adds confusion for me. I don't like that it is called View. it's actually a template loader ... and it's more centric to the item than the html ... so trying to think about "I want the item data" when dealing with a View object is ... confusing ... maybe it should be an `Item` actually??? no ... idk.  
  
To make it work, I added code to `phad-routes`, to `Submission` trait, to `Compiler`, and to `View`.   
  
... one problem i have is ... phad modes ... I don't know an easy way to add a new phad mode ... and I guess this isn't designed to make it easy to add new phad modes ... But it would be nice if this were easier... I've been thinking linearly about phad modes execution when I probably shouldn't be. The different modes are very different outputs, all smashed into the same compiled file. I don't mind them being together, but they are different branches & I'm not sure I like how I'm handling & thinking about those branches.  
  
  
## Jan 6 end of day  
I'm trying to work on the `deleteable` feature for forms, but my brain is not seeing the big picture, so putting that on hold ... So, there is currently no implementation for deleting items. It is trivial to delete an item from the database. I'm confused about how to integrate deletion with access & I think that confusion is making it hard for me to work on any other parts of this. I need a unit test that doesn't do access that JUST deletes an item ... this function, i THINK, would be called from thew view file itself ... I don't like doing this IN the view file, though ... bc it's not what the logic is setup for ... like loading the item list? Well ... I guess that does make sense ... if you want to load the item into memory before running the delete query ...  
  
I think I need some unit tests  ... for the lowest level (simlpy deleting an item from the database), then for ... idk ... bc idk how I'm doing access & idk how I'm routing it & bc I'm so unsure about those things ... this is a tomorrow issue.  
  
## Jan 6  
- DONE unit tests loading form object by id & loading empty object for form  
  
### `deleteable` form flag  
- how does that relate to `<access>`? The same access granting mechanism that applies to loading a view applies to submitting a form & likely will apply to deletable ... this is a problem ... I NEED different rules for viewing and for deleting ... OH, but view form & submit form are the same `<access>` whereas viewing that same item in a different view (non-form) would have a different access ... so it basically just says "if you can view this form, you can submit it & you can later delete what you submitted" ... I'm ... kind of okay with that, but it isn't great.  
    - then there's the question of more complex access methods ... right now, I'm just testing overriding the user to determine access or not ... i need a test that checks access another way ... then I'll return to this deletable issue bc it might help me figure out if I can differentiate these  
  
### TODO  
- server test: decline/approve access based upon custom access function  
- server test: decline/approve access to submit form based upon user role  
    - maybe also ensure access to viewing the form is declined ... this is intended to test somebody (somehow) knowing the submission API even if they can't view the form ... say if someone scripts submission & shares that script with others who are not authorized  
  
- DONE add `<form>` node info (action, method, enctype)  
- DONE an api route to delete items (if a flag is set like `<form deletable item="Blog">`)  
  
- DONE server test: decline/approve access based upon role  
- DONE server test: access-declined error message  
- DONE server test: access-approved custom handling  
  
  
Then:  
- refactor routing ... I want it to be mostly independent of Liaison then have very short, very simple integrations for liaison.  
    - may need to look at notes below for more info.  
  
Later:  
- javascript-based form submission  
- look at old notes?  
- modal stuff  
- idk  
- caching stuff ...  
  
## Jan 5, 2022 end of day  
NEXT: see jan 4's todo.  
- added file uploads  
- add loading form item by id (from GET)  
  
Notes: I don't love my implementation of the form-specific handling... It's all kinda messy & intertwined & I think earlier on I should check what kind of loading request this is ... to see whether I'm loading for a view or for a form, then route the loading based upon THAT ... what I'm doing currently is using the same logic for both & adding a couple conditionals for loading a form or for display_with_empty_object or `phad_mode=submit` ... Rather than sharing logic, I'd like them to be routed early on & then each have their own logic. Refactoring this is NOT a priority.  
  
I need to test `<access>` more ... actually test whether access can be denied based upon ... whatever whatever ... I don't have any server-side tests for this. I also need to test `<on s=404>` and other status nodes ... I could use an `<aftersubmit>` node, an `<itemwillload>` node ... but those probably aren't a priority.  
  
## Jan 4, 2022 END OF DAY  
It works! I did:  
- setup routing for form submission  
- setup redirect on form submission  
- add item_type='form' to ItemData for forms ... 'view' for views  
- tests in `test/run/server/Main`  
- db bootstrapping before all tests run (rather than ON the server request)  
- verified submission + redirection + display submitted blog post works  
  
TODO:  
- DONE display a pre-filled form from existing item... use `$_GET['id']`  
- DONE(ish) upload a file  
    - ISH ... `test/Server/phad/form/document.php` has `<onsubmit>` code that handles the file upload. It also declares two backend nodes ... Basically, I just want to genericize what I'm doing there & put it into Phad proper. I don't think it'll be that hard, but my brain is mush right now. Umm. I also had to manually set the method & enctype bc that's not implemented automagically yet  
- add `<form>` node info (action, method, enctype)  
- an api route to delete items (if a flag is set like `<form deletable item="Blog">`)  
  
Then:  
- refactor routing ... I want it to be mostly independent of Liaison then have very short, very simple integrations for liaison.  
  
Later:  
- javascript-based form submission  
- look at old notes?  
- modal stuff  
- idk  
- caching stuff ...  
  
## Jan 4, 2022  
- MAYBE rename `<access>` to `<data>` ...   
- Server tests TODO  
    - DONE display item via query  
    - DONE display view containing nested item  
    - DONE get item as json   
    - display a form  
        - pre-filled from existing item  
        - DONE not filled  
    - DONE submit a form  
    - upload a file  
  
### Forms  
I thought forms were more ready than they are. ... I need action, method, & encoding added to the html `<form>` node. To display a form, I use `$view->outputWithEmptyObject()` ... and I'm not sure there's any other indication that it's a form ... So i need to setup routing for it ... but how do I know that it's a form, in order to route it differently and call `outputWithEmptyObject()` instead of the default `output()`??  
  
So there's routing ...  
- route for displaying with empty object  
- route for pre-filling from an object's id  
- route for submitting  
- route for deleting!  
  
and there's information on the `<form>` node.  
- action  
- method  
- encoding multipart/formdata (when there's a file, at least)  
  
Potential other features  
- auto-route for deleting items  
- javascript based form submission  
  
I'm trying to decide where to start: with the action/method or with the routing ... The routing isn't necessarily completely helpful if the action/method aren't set but ... I'm working on routing stuff today & I want to keep working on it, so I'm gonna do that ...  
  
THEN once I have the routing working, I'll probably mess with the action/method/enctype  
  
... so submission is working, but i have no way to verify it ... options  
a. in the routing just do a query & return the blog names to make sure the update completed  
b. add a 'didsubmit' node to compilation, then implement it, printing a list of blog names  
c. redirect to form target ... set the target as `/blog-names/` & then curl will (hopefully) get the blog-names response ... this still won't work, because the server will be initialized again & the newly inserted data will be gone (though I WILL need to verify redirection)  
  
Maybe the way I'm handling data in these tests needs rethinking ...  
  
Maybe I need to init the data during testing bootstrapping, then use a stored sqlite file throughout all of my tests, then I can do the redirect & stuff ... ugh ... yeah ... probably  
  
## Jan 3, 2022: TODO   
- DONE update to use new liaison  
- DONE test sitemap generation  
- Liaison Server Setup (finish writing prototypes as needed, then integrate into Phad Proper, likely as a separate controller class for liaison integration)  
    - `test/Server/phad-routes.php` sets up routes. Move this into phad proper (likely as a separate class for integration)  
    - `test/Server/phad-sitemap.php` creates the sitemap.xml, adds a route to /sitemap.xml & delivers it  
        - I may want to let `<sitemap>` nodes name their sitemap file or at least allow SOME kind of configuration for this.  
  
- **NEXT** Liaison Server Tests  
    - I have some very simple tests that verify routing & sitemap generation. I need to just start testing the other features of Phad this way ... querying for item data, querying for sitemap, get json data, nested items, forms, form submission, file uploads, etc ...  
  
  
### Jan 3, 2022  
- DONE finish sitemap builder & its unit tests, primarily functionality of get_entries()  
- DONE added some sitemap builder unit tests  
- DONE added more formvalidator tests  
- DONE minor cleanup, change names of test classes  
  
## Dec 24, 2021 (#2)  
- See `Devlog.md` for old TODOs, known bugs, and known security issues  
1. Finish writing unit tests (and refactoring)  
    - See Testing TODO from Dec 22  
    1. DONE Sitemap refactor + tests  
    2. DONE inserting/updating DB data  
    3. DONE filters? (if I care to bother)  
2. Consider other tasks:   
    - create a server test setup & write unit tests for:  
        - submitting forms (and redirecting)  
        - submitting files  
        - loading forms (via `<route>`s)  
            - pre-filled  
            - failed-to-submit  
            - empty  
        - loading views (via routes)  
        - getting data as json  
        - get sitemap(s)  
3. Consider unwritten/unfinished features (see Devlog.md, Dev-Modal/, MAYBE .old/, MAYBE .docsrc)  
    - load a modal  
        - javascript integration  
    - Api feature like `<button api="Blog:create" role="admin">New Blog Post</button>`  
        - enabled by javascript? Maybe special handling for certain nodes, like js onclick for a button, but use href="url" for an `<a>`  
    - individual node access like `<a href="/admin/" role="admin">Admin page</a>`  
    - look at Devlog.md for other features that I've forgotten about, but need to write.  
  
### Parts:  
- Compiler:   
    - Builds html + php from phad-html  
    - extract `<route>` data  
  
## Dec 24, 2021  
Acceptance. I fear I must accept the "poor" design of the access class. It's kind of messy. It deals with the `$Item` and `$Access` objects (which I don't like), but it ... works. I've tried a more functional approach where only the necessary params are passed to the hasAccess() & loadItems() methods but ... it's a mess. Then my StatusBasedContent test was failing ... Well its override of hasAccess() needed the `$Access->index` which brought up a very likely use-case ... explicit overrides of the access used based upon SOME thing that isn't integrated into PHAD. So ... I think `hasAccess()` needs to keep the $Item and $Access. `loadItems()` could probably do without but ... at this point ... there's no reason to care. I could spend time refactoring loadItems() to be more functional & clean, but the `resolve()` is going to be more complicated.  
  
I may want to clean this approach up in the future, but I don't know how. I would need a significantly different solution. Maybe I just need to turn Item & Access into classes so there structures seem more rigid. I don't know. I don't think I care that much right now. I need it to WORK & be easy to work with ... not be perfect.  
  
  
## Dec 23, 2021  
I'm refactoring the Access class. `resolve()` is still a large function that's confusing. I want `Access` to have zero knowledge of the `($Item, $Access)` stuff & instead only receive the specific things it needs. Phad should have awareness of the data structures it creates for compiled views ... but the utility classes like Access & Query should not be. This cleaner approach makes things far more testable & WAY easier to understand.  
  
I moved the item list loading OUT of hasAccess() ... it is now called in resolve() as part of `hasAccess()&&loadItems()`... loadItems still uses `$Item`, and `$Access` & I think `hasAccess()` does too. I want to change that.  
  
`loadItems()` is just a nice little wrapper around `Phad\Query` that makes it work with the confusing access-flow.   
  
So two options:  
1. in the compiled view, instead of calling `resolve()`, call a func to check access & another to load items.  
2. Move `resolve()` into the phad controller. Loop over the access list & call `Access->hasAccess()` for each of them. Also try loading items from `Query` (which means moving loadItems into query??)  
  
I'm not exactly sure what I'm doing here. I just know I want it majorly cleaned up & I'm already headed in that direction.  
  
I'm interested in changing some of the compiled-output. I'm happy with most of it, but some of it is ... meh. However, changing the compiled output is the LOWEST priority. And can only be done after the rest of the refactoring is finished (unless it's a minor change that won't cascade throughout the software) ... So while I may want to change the information structures ... I'd rather just slap some ugly code in `Phad` & have it deal with the yucky information structures .... I'd rather do that than spend many hours refactoring just for the sake of cleaner code. I don't NEED the compiled output to be any different in order for this program to be easily maintained & improved. I'm already working on the refactoring which achieves that goal.  
  
Thanks,  
Bye  
  
---  
  
- made a Query class & refactored & wrote tests for it  
- `Access->resolve($Item)` is an awful method. I want to refactor this so the interface with `$Item` happens in `Phad`, then it just nicely calls `$Access` to test individual accessors. It's okay the way it is ... the concerns are fairly separated. loading an item list is still nested inside the `resolve()` flow, but at least the actual loading of item lists is separated into their own class & the querying itself is tested  
  
## Dec 22, 2021 (with some minor changes up to jan 7)  
  
### Testing TODO  
Distinct functionality that can be unit-tested withOUT Phad present:  
- DONE insert-into/update a database from an array of key=>values   
- DONE build a sitemap:   
    - DONE take in a pattern, sql, and other sitemap attributes  
    - NIX add custom handlers to modify sitemap data on a row-by-row basis  
    - DONE generate sitemap xml    
    - DONE write xml to disk  
- Modify values via named filters  
  
### TESTING DONE  
- DONE grant access to a user if they match a role & data is available for the query  
- DONE perform queries based upon array of 'sql', 'where', and other verbs mixed with passed-in args (requires query compilation, including figuring out what keys to bind)  
    - NOPE query-building and query execution COULD potentially be separated ... BUT I don't think I wanna  
- DONE building a data array from nested loops of data-lookups (the stack)  
- DONE compile phad-html into php + html  
    - can NOT fully verify without having phad present, though I can compile & verify certain aspects without phad  
- DONE (minimally) validate an array of values against an array of specifications (like minlength, etc)  
    - specifications come, in part, from existing html input declarations  
  
  
## Dec 21, 2021  
- stack is working & tested  
- found `Dev-Modal` folder for js & front-end stuff  
- created devlog & cleaned up status.md  
- start on compiler  
  
  
I'm working through tests one by one and making them all pass. Currently working on `Forms` tests. do `phptest -class Forms -test SubmitInvalid` ... That's wher I am.   
  
I just kind of hacked together some of the old submit() code. I might need to do some thorough review ... partially because I'm not sure how it's supposed to work... the submit() call is supposed to output a form, i think ... just filled with the submitted data. But right now the submit() call is not returning anything. (I also might have left some valuable submit() code back on the old controller)  
  
I'm a little confused about `FormValidator` and `PDOSubmitter` ... I think these are NOT classes for the controller, but are utility classes. Then I think I need controller classes that invoke them. But I don't know. The whole flow for submission needs reviewed ... idunno.  
  
After that, probably Routing tests ...  
  
I have one package test and several component tests ... I suspect these will need to be removed and have new liaison-integration tests written... idunno yet  
  
Documentation tests are all failing, but those are the least priority  
  
I'm frustrated with a lot of these tests. Many of them are massive integration tests that require phad, view compilation, and are just doing so much. I want more unit tests that specifically test the individual controllers & classes without a phad present or view compilation ... then sprinkle in SOME integration tests  
  
## Dec 20, 2021, end of day  
the stack is a MESS. I'm trying to figure out the JsonApi tests & I can't get the stacking to work. Currently, nested objects work & one object works, but adjacent objects don't! And I have no idea why. The whole stack thing is so so confusing. Maybe I should use a different design alltogether? If not, when I get this working, I need to write good notes IN THE CLASS  
  
  
## Dec 17, 2021, end of day  
I reviewed PHAD at length, wrote down many (most) of its features & described how they work. I described the logical flow. I reviewed classes & wrote down what each of those do. I wrote down different nodes & node-properties. I jotted down different handlers. I wrote notes about issues I have with how things are currently.  
  
I do NOT have a clear proposal/idea of how to fix the problems. It works. But the DevX is awful. Which means I don't use it. It means I can't update it. It means that it takes me 2+ hours to figure out how it works & I still don't know how to do anything really. The API is bad.  
  
Anyway. Look at the `Architecture.md` document & READ IT. Then start making a plan or thinking about what to do or prototyping some changes or ... something. Do this on old-liaison. Converting to new liaison with how things are currently would be a freaking NIGHTMARE, so I want to clean up the project, THEN convert to new liaison.  
  
It looks like most of the features are ready to go.  
  
  
## Dec 17, 2021  
- 4 tests are failing: PackageRouting, ManualRouteAdditionThenRealDelivery, ManualRouteAdditionThenDeliverySample, and Documentation::Routing. These were failing during past development. I intend to fix tests AFTER updating to the new liaison  
- I'm reviewing the architecture, trying to get a sense of how everything works. I want to refactor using the old-liaison so it's easier to follow. THEN I want to convert it to new liaison.   
  
## Current  
- Trying to write Routing documentation test and... everything is breaking & I don't understand.  
    - If I comment out `$lia->set('phad:routes.alwaysBuild', true);` in `Documentation->testRouting`, then run `phptest`, another test fails. Idunno why. Maybe figure it out?  
  
# Structures (from march 11 & april 6, 2021)  
EVENTUALLY this section should be moved to its chronological position. But for now, it might be helpful in devving, so i leave it at the top  
  
This is a starting point for "what do I do next?"... If I've run out of things to do or am otherwise stuck, then I should probably review this file & pick something from it to work on.  
  
## Stuff  
- individual node access  
    - Like `<button api="Blog:create" role="admin">New Blog Post</button>`   
    - Does this use a `foreach` loop, too? Yes.  
      
### The $controller var  
How can the controller class be selected?  
- do nothing, use default class  
- In `config.json` write `"phad:controller.class":"\\The\\Cool\\Controller"`  
- In `package_dir/` create `PhadController.php` & implement `Phad\Controller` or something for a package-wide controller  
- Create `package_dir/phad/ViewName/Controller.php` implementing `Phad\Controller` for a view-specific controller  
  
### Writing Tested Documentation.  
- DONE Sitemap Generation (in progress)  
- Routing  
- Access controls  
    - role-based  
    - query-driven  
    - php handler  
- Form  
    - Basic submission  
    - `<onsubmit>`  
    - submit handler  
  
## Next   
- Package: make the package setup sitemap?  
- Sort the Phad\Controller code into traits for:  
    - Sitemap  
    - Submissions  
    - Access  
    - data-building (rowStarted, rowFinished, etc)  
- Views: enable `sql` attribute for specifying the entire query on a view  
- Tests I might already have  
    - Tests: Test that passing item works both for passing an array & passing an object   
    - Tests: Test role-based access  
    - tests: Test named-handler based access  
    - Tests: A view with only php code  
- Allow `return false` to be a valid action to stop a form submission.   
    - Form won't finish submitting because the compiled script has been short-circuited  
    - The calling code will `ob_get_clean()` & throw away any output from the compiled script  
    - The calling code will then display the form (not trying to submit it)  
- Using filters for custom submission  
    - Ex: Form has `<input type="text" prop="title">` & wants slug. Add `<input type="computed" from="title" filter="slugify" to="slug">`   
- Prevent output from `<onsubmit>` node contents. maybe?  
- The test using `BlogWithOnSubmit` SHOULD fail, because it doesn't list the (????)  
- Submit files  
  
  
# Forms (april 6th, 2021)  
  
## Form Submission (internals)  
- visit form page  
- view is compiled. Submit requirements are compiled  
- Form is filled out & user presses submit  
- js `onSubmit(form, data)` is called (return `true` or `false` for 'do submission' vs not)  
- request is sent & server receives  
- The custom router is invoked  
- The compiled view is `require`d with the 'dosubmit' paramater.   
- Access is done, just like usual. Then if granted, the form is submitted.  
  
## Form Tidbits  
- Forms can not be nested in other forms  
- Forms cannot be nested in views. (its possible this would work, but I'm not guaranteeing it)  
- Submission Handling:  
    - `<onsubmit>` to declare a submit handler inside the view  
    - override `Phad->onSubmit()` to do additional submission handling  
  
## `<onsubmit>` node  
While you can put html/text or other content to display here, the intent is to run PHP code just before `$phad->submit()` is called.  
```html  
<form item="Blog">  
    <onsubmit><?php  
        $BlogRow['slug'] = str_replace(' ', '-', strtolower($BlogRow['title']));  
        /** set phad_mode to anything other than `submit` to skip submission (form will be displayed). */  
        if (false)$BlogItem->phad_mode = 'cancel_submit';  
    ?></onsubmit>  
    <input type="text" name="title" maxlength="75">  
    <textarea name="body" maxlength="2000" minlength="50"></textarea>  
  
    <input type="backend" name="slug" minlength=4 maxlength=150 />  
</form>  
```  
  
## Old stuff ... from december 2021?? I think it came from the status doc  
  
## Todo - High Priority (has to be done before release)  
- Front-end integration  
    - See Forms.src.md for refresh on form flow  
    - Modals  
        - See Status/modal-stuff  
    - api declarations on individual nodes (like clicking a button shows a modal to edit a blog post)  
        - See Status/ideas/QnA.md   
        - & maybe review Status/modal-stuff  
  
## Todo - Medium Priority (probably won't do it)  
- Output multiple sitemap files & an index sitemap file listing all of them  
- use modtime to determine if a view needs to be re-compiled. When compilation is done, set the modtime to be the same on both source template & compiled output. See php `touch` function.  
- schema.org stuff  
- Listing columns explicitly in `SELECT` statements  
    - implement by scanning item data for `properties`  
    - Add `columns` attribute to include more properties  
        - OR add a special node that disappears when compiled, but adds the property to the `properties` array  
        - Are these columns publicly available? (on a JSON request, for example)  
  
## Latest (new to old)  
- Write tested documentation for Sitemap generation  
- make declared `priority`, `last_mod`, and `changefreq` work  
- Implement sitemap handler feature & test  
- Test sitemap generation (including write-to-disk)  
- Implement sitemap generation  
- Fix `accessStatus` bug (when no accesses allowed access, the status WAS still 200. Now its 404)  
- Fix accessIndex `else` code (compiler stuff)  
- Sitemap generation  
    - Plan it   
    - Start a test  
- Make filters extensible  
- Test filters & document writing your own filter  
- Test that multiple routes work in a view  
- Package-wide routing, building routes from all views and caching them.  
- Implementing Phad Package  
    - Refactor package setup in Liaison  
    - Implement crude setup function to load all phad views & their routes  
- Implement Routing  
    - `<route>` tag  
    - Getting route data from compiler, view  
    - Delivering a route via liaison with a test implementation  
- Failed submissions can auto-report error message wherever `<error></error>` node is present. `<error>` node is removed.  
- Remove `<onubmit>` tag from the compiled code.   
- Extraneous columns:  
    - Disallow them  
    - Add `<input type="backend">` for extraneous columns  
    - Auto-add `id` to properties, only if `<input name="id">` is NOT set.  
- `phad:pdo` does not need to be set if an object is passed. (did this awhile ago, but not sure when)  
- implement `<onsubmit>` in the form  
- implement `onSubmit()` in controller  
- Form submission validation for `<select><option><option><option>` lists  
- Form submission redirection: Target is the blog post's new page, so the url has to be created dynamically.  
- Forms: Return proper redirect headers  
- UPDATE data from submission  
- Convert current db operations to use LilDb  
- Insert data from submission  
- Compiling in form-submit data (properties, requirements for each)  
- Write unit tests for the validation system...  
- Refactor test views  
- Decide how form submission is going to work  
- Clean up json api notes  
- Implement data extraction from phad views (for the json api feature). Tests passing.  
- Implement nested item support, including several tests  
- Fix bug when only status node is status=200  
- Fix indenting on compiled code  
- Clean up unused code from compiler  
- Refactor compiler (status-code stuff)  
- Write tests for status-code stuff  
- Handling of status codes (`<on s=404` or `<on s=200`, etc)  
- Create a `$BlogItem` that encapsulates ALL information, including a list of accesses & the result of processing those accesses. It might eventually include the routes as well.  
- Fix bugs related to updated compiler  
- Update the compiler to produce the new output (see Status/SampleCompilation.md)  
- resolveAccess() & hasAccess() & dependent functions (query, role, & user getting) are written  
    - Tests all passing, too  
- Clean up notes in this file & `structures.md`  
- Figure out the `$controller` var in a phad view  
- Fill in the rest of the desired compiled output  
- write source code for desired compiled output  
- Internal: Array representation of `<access>` figured out  
- Wrote a component class with a `view($viewName, $args)` function that:  
    - has tests  
    - compiles the named template from source file  
    - Outputs compiled template to disk  
    - passes args to the compiled template  
    - returns an object representation of the template (like existing view class).   
    - handles `$this->` calls from the compiled view.  
- Test that uses databasing via the compiler  
- Add option to pass BlogList (instead of just `$Blog` object).  
- Switch compiler output to a foreach() approach  
- `<select>`/`<option>` setup, to set the `selected` attribute on the correct `<option>` from the input `$Object`  
- Refactor the existing node-replacement-code  
- Tests passing for:  
    - compiling pHtml with `item` & `prop` & `filter` attributes  
    - Removing `<x-item>` tag (but keeping its children)  
    - Eval compiled pHtml with a static item. (i.e. a `$Blog` object, not a query)  
    - `<form` with `name` instead of `prop`.  
- Deleteted most old code & some notes  
- Split it into multiple files  
- Wrote pseudo-code  
  
  
## Known Issues  
There are no plans to "fix" these. Just things to work around.  
- The phad controller has a property listing columns that failed submission. This could lead to some weird conflicts, especially if there are nested view calls (as I think they'll all be using the same controller).  
- `$phad_mode` only works with the first item.  
- `phad_mode=='submit'` code is present in non-form views. It's useless & adds unnecessary overhead.  
  
  
## Known Security Issues  
- If a `backend` property is given in a form, & the user submits that property, AND the backend code that normally generates that property value fails, then the user-submitted property value would be considered passing.  
    - To prevent this from being a vulnerability, template code can ALWAYS set the property, even if there is no valid value. OR on fail can `unset($ItemRow['propertyName'])`  
    - Fixing this requires some pre-onsubmit-validation. Some early validation. I'm not currently setup for this, so I just need to stay mindful of this.  
    - A very simple solution could `unset($data['propName'])` prior to any looping  
  
  
## Notes  
- Can an `item` also be a `prop`? I think its a great idea, but not good for an MVP.  
- In a form, an input could have `name` and `prop`, basically to make `name` an alias for `prop` on the object & in the db. In case I have a gross property name & want a cleaner form input name or something.  
- I should stop calling them `views` & come up with a different term. They're not JUST views. They're also access-controls, routes, apis, database definitions, validation specifications...  
  
## Todo? Not Sure This is actually a TODO...  
- Review the rest of my 'old' code & notes & translate it into a more streamlined spec or better examples or something.  
- a 'getUser' function... Think 'UserMap', except way simpler because its just provided by the controller.  
- Declaring a PHP Handler for a package / for a view  
- Configuration file (namespace, view directory, base url for routes, global view handler? ...)  
  
## Todo? Low Priority  
- A way to find views based upon the item name / table name. Ex: A form submitting a `Blog` could `$phad->routeFor('Blog:view')`, then redirect to that view's page.  
- one-time use key pointing to an array of hidden values... Such as completing user registration. You might use a placeholder value instead of the actual registration-confirmation value that came from the link.  
- a `boostrap.php` file to do any desired setup for the package  
  
## Todo? No priority  
- Squash nested item queries into a single query  
- A way to modify `ItemData` from the calling code. Like a special key=>value arg that gets iterated & values copied into ItemData  
- `<accessgroup>` to group multiple `<access>`es together & only pass if all of the child accesses pass  
    - Compiled Output example: `$BlogAccessList[] = new \Phad\AccessGroup($access1, $access2, $access3);`  
    - Problems:   
        - Who defines the query, then? The group or one of the individual accesses?  
        - `<access>` wouldn't support the same features inside an `<accessgroup>` that they do normally.  
        - How is the access group validated? It requires different handling than a single `<access>`.   
    - These issues are resolveable. But I don't think I need access group. In these cases, I can just use a php function & do the access programatically. Maybe not as clean & nice, but it would work & would save me development time compared with architecting access group & implementing it.  
    - I can always figure out how to add it later.  
    - OH OH: Each `access` data array could have an attribute specifying its access group & number of accesses... or something  
        - This way, the access knows its part of a group... idk  
- Configurable node names. In case you don't like `<access>`, maybe you want `<x-access>` or something, but you still want it to function the same.  
- error handling with multiple accesses. Ex: 1st 2 access nodes return 403 & 404 respectively. 3rd gives 200 ok.   
    - Ignore the 403 & 404. Respond using the 200 ok.  
    - `<access log_error="always">` to always log the error (or something better)  
    - `<access throw_error="always">` to always throw the error  
    - `<access continue="true">` <- If this access is reached, it must pass in order to try the following accesses.  
        - This is an alternate approach to `<access-group>`.   
        - Ex: 1st access: `<access name="IsNotABot" continue="true">`. Then: `<access role="admin">` & `<access role="moderator">` are both declared & do slightly different things. `IsNotABot` HAS to pass. THEN either `admin` OR `moderator` needs to pass. Otherwise, an error is given.  
- Custom PHAD components... This is not a priority.  
    - Kind of like how the Phad component is a Liaison component. Lia\Compo gives cool functionality. Phad\Compo gives additional cool functionality.  
  
