<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
  
class TaskAdder extends ModalHandler {  
  
    onAttach(column){  
        super.onAttach();  
        this.column = column;  
    }  
  
    onError(){  
        alert("There was an error with auto-wired modals & submissions");  
    }  
    //I think there should be onRequestError, onSubmitError, and onResponseError  
    //but maybe onError could handle all three? but then there needs to be a way to check what the error is coming from  
  
  
    //this is for REQUESTING the form  
    onRequest(requestData, initiator){  
  
        console.log(requestData);  
  
        //requestData is all request params that have already been set whether with this.setRequestParam() OR from param-colname="value". So it should be an empty object right now  
          
        //I SHOULD put this in onAttach, because it's not going to change between requests. BUT, I want to try it all out  
        // with the generally intended usage  
        // also this===initiator, so I don't need to be using initiator...  
        this.setRequestParam('column',initiator.column.id); // this would be sent to the server when requesting the modal  
  
        return [initiator.column];  
    }  
  
    // this happens BEFORE the form data is uploaded   
    // There's GOTTA be a way to modify the upload data here...  
    onSubmitData(data, targetUrl, column){   
        console.log('onsubmitdata was called yay!');  
        data.title = `changed on client ${data.title}`;  
        const newTask = column.addTaskFromJson(data);  
        return [newTask];  
    }  
  
    onResponse(responseData, submitData, fromUrl, taskInGui){  
        // taskInGui.remoteId = responseData.remoteId || 'remote id not found';  
        console.log({response_data:responseData});  
        console.log(taskInGui);  
        alert("We added a new task!!!");  
    }  
  
}  
// gets added by the column, instead of registering itself  
