# Frequently Asked Questions and ... stuff
Questions & other notes that I want to keep around

## Usage Notes
- add `candelete` attribute to `<form>` item nodes to enable deletion
    - then request `/route/for/form/?phad_action=delete&id=id_to_delete`
- `POST`ing with an `id` to an item uses an `UPDATE where id=:id` query instead of an `INSERT INTO`
- Uploading files: See [test/Server/phad/form/document.php](test/Server/phad/form/document.php) for an example. It's not quite integrated & does require some work.

### Managing data in views
- bindable paramaters in an `<access>` node pull from `$NamedItem->args[$key]` where `$key` is a string like `:id` or `:slug`
- if no paramaters are passed to a view (and no access nodes are defined), then `SELECT * FROM item AS Item` will be executed
- `$phad->item('BlogView', ['Blog'=>$an_object])` will show the passed in blog post & nothing else (there is no way to disable this functionality currently)
    - Alternatively, pass `['BlogList'=>[$obj1, $obj2, $obj3]]` to show multiple blog posts of your choice.

### nodes
- `<route pattern="/blog/{slug}/"></route>`
- `<sitemap sql="SELECT slug FROM blog" handler="ns:func" ...>`:  contains other declarations for the sitemap like priority, last_mod, changefreq, etc. the `ns:func` must be registered with `Phad\Addon::addSitemapHandler('ns:func', function(){})`
    - must be inside a `<route>`
- `<access name="whatever" handler="ns:handler" role="user_role" where="Blog.slug LIKE :slug">`
- `<on s=404>` or `<on s=200>`: display different messages / run different code based upon the access results. CAN be inside an `<access>` to limit it's scope, or outside the access to apply to all accesses.
- `<x-item item="Blog">` a node that gets removed while allowing all PHAD features to work.
- `<x-prop prop="title">` a node that gets removed while allowing the prop to be printed
- `<onsubmit>` only for forms

### node attributes
- `<input>`
    - `prop` to fill value
    - `type` may be "backend" to allow a prop to be generated on the backend
- Item Nodes
    - `item="Blog"` declare a node as an item 
- Property Nodes
    - `prop="title"` to fill the node with the property
    - `filter="filter_name"` to modify the prop's value before printing

### ItemInfo (`$BlogItem`)
`$BlogItem` or another item ... an `Item` is created to hold information. If the `item="Blog"`, then there will be an object `$BlogItem`
- `$BlogItem->name` is `Blog`, as declared in `item="Blog"`
- `$BlogItem->accessList = []` & has data added from `<access>` nodes, such as the query
- `$BlogItem->accessStatus` is `false` & is set `200` if access is granted, `404` if no items are found, (I think it does others, too)
- `$BlogItem->accessIndex` is the index in `$BlogItem->accessList` that was approved for the current user
- `$BlogItem->args` is the `$args` passed in by `$lia->view('view_name', [...args])`
- `$BlogItem->phad_mode` is `$phad_mode??display`
- `$BlogItem->list` WILL contain the array of items, if access is granted. This list is looped over to fill the view's html
- `$BlogItem->properties` ONLY set for forms and contains information from the prop-nodes like `tagName`, `type`, `minlength`, and more

### `$phad_mode`
Pass `$phad_mode` to a view to determine the response
- `display`: default, will show the view
- `get_routes`: returns an array of routes, as an array of attributes from the `<route>` nodes 
- `get_sitemap_data`: return array of sitemap data, as copied from the `<sitemap>` nodes
- `submit`: submits a form by calling `$phad->submit($BlogItem, $BlogRow)`
- `get_item_data`: load the `$BlogItem` as you would for displaying, but just return it.
- `display_with_empty_object` will display the form passing a `BlackHole` instance so all property values are empty
- `delete`: delete an item, as long as `$_GEt['id']` is set (or `id` is otherwise added to the view's args array)

## Responding to Requests
- `deleteable` responses
    - set `deleteable="goto:/some/url"` to auto-redirect on SUCCESSFUL deletion
    - set `deleteable="print:Some message"` to auto-print a message on SUCCESSFUL deletion
    - set `deleteable="command:some_command_name"` and `$phad->delete_commands['some_command_name'] = function(bool $didDelete, object $ItemData)` to call a custom command whether deletion succeeds OR fails.

## Sending Requests
- Delete an item: `GET /item-form-url/?phad_action=delete&id=7` where `id` is the id of the item to delete. The form MUST have `deleteable` attribute

## Custom Handlers
- `Access` on items
    - set `<access handler="handler_name">` and `$phad->access->handlers['handler_name'] = function($ItemData, $Access)` (identical to `hasAccess()`)
- `Access` on nodes (not items)
    - set `<div access="role:thisone|orthatone">` and override `$phad->access->user_has_role('thisone|thatone')` if you want custom implementation of that.
    - set `<div access="handler:can_access_div">` set`$phad->access->node_handlers['can_access_div']` to something like `function(array $node_info)` where `$node_info` is like `['access'=>'handler:can_access_div', 'tagName'=>'div']` and would also contain any other attributes, the same way it does for `access` attribute.


## Known Issues
There are no explicit plans to "fix" these. Just things to work around. They may be addressed in a future version ... who knows?

## Jan 14, 2022
- File uploads are not properly implemented... it requires custom `<onsubmit>` code & is a little messy. I intend to fix this eventually, but it is not a priority.
- There is no intentional handling for duplicate routes. In scanning for all routes, the last-found route wins

### Jan 6, 2022
- The access status / access index logic flow is really messy. The compilation for that flow of logic is even worse. This ... needs to be significantly refactored ... but ... it's not a priority because it WORKS & refactoring will probably be a significant endeavor.
- `<form action="">` is added automatically to forms if action not set ... but the action should probably respect a defined `<route>` if one exists. Workaround: Just explicitly define the action on your form node.

### Jan 3, 2022
- `SitemapBuilder` may overflow memory. Current implementation gets all entries as array before writing xml to disk. If the resultset is large, this may cause a memory overflow. It should be refactored to fetch one-row at a time & immediately write to disk before doing the next row. 

### Dec 24, 2021
- `Access` class has `resolve($Item)`, `hasAccess($Item, $Access)`, and `loadItems($Item, $Access)` ... This is poor design IMO, but "fixing" it is too much of a headache & reduces extensibility, given approaches I've considered so far.

### early/mid 2021
- The phad controller has a property listing columns that failed submission. This could lead to some weird conflicts, especially if there are nested view calls (as I think they'll all be using the same controller).
- `$phad_mode` only works with the first item.
- `phad_mode=='submit'` code is present in non-form views. It's useless & adds unnecessary overhead.

### Security Issues (early/mid 2021)
- If a `backend` property is given in a form, & the user submits that property, AND the backend code that normally generates that property value fails, then the user-submitted property value would be considered passing.
    - To prevent this from being a vulnerability, template code can ALWAYS set the property, even if there is no valid value. OR on fail can `unset($ItemRow['propertyName'])`
    - Fixing this requires some pre-onsubmit-validation. Some early validation. I'm not currently setup for this, so I just need to stay mindful of this.
    - A very simple solution could `unset($data['propName'])` prior to any looping

