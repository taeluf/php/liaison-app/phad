<?php

$phad = $lia->get('phad');

$NameList = [
    [
        'name'=>'One',
    ],
    [
        'name'=>'Two',
    ],
    [
        'name'=>'Three',
    ],
    [
        'name'=>'Four',
    ]
];

$view = $phad->item('explicit', ['NameList'=>$NameList]);

echo $view;
