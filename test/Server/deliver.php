<?php

set_error_handler(
    function($errno, $errstr, $errfile, $errline) {
        throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
    }
);


$start = microtime(true);

require(__DIR__.'/../../vendor/autoload.php');

$dir = __DIR__;
$lia = new \Lia();
$server = \Lia\Package\Server::main($lia);
// $server->addon('lia:server.server')->useTheme = false;
// $server->set('server.useTheme', false);
$lia->set('lia:server.server.useTheme', false);
$lia->set('lia:server.router.varDelim', '\\.\\/\\:'); //default includes a hyphen, which is dumb as hek

$main = new \Lia\Package\Server($lia, 'main', __DIR__);

$router = $lia->addon('lia:server.router');

$lildb = \Tlf\LilDb::sqlite(__DIR__.'/db.sqlite');

require(__DIR__.'/phad-setup.php');

$setup = microtime(true);

$lia->set('phad', $phad);
$lia->set('lildb', $lildb);
$lia->deliver();

$end = microtime(true);


$setup_bn = $setup - $start;
$deliver_bn = $end - $setup;
$full_bn = $end - $start;

// echo "Start: $start\n"
    // ."Deliver: $deliver\n"
    // ."Full: $full\n";
// echo "Setup: $setup_bn\n"
    // ."Deliver: $deliver_bn\n"
    // ."Full: $full_bn\n";



