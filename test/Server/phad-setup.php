<?php


$_GET['user'] = $_GET['user'] ?? 'default-user-role';

$options = [
    'item_dir'=>$dir.'/phad/',
    'cache_dir'=>$dir.'/cache/',
    'sitemap_dir'=>$dir.'/sitemap/',
    'pdo' => $lildb->pdo(), // a pdo object
    // 'user' => require(__DIR__.'/phad-user.php'), // a user object (no interface available ...)
    'router' => $router,
    'throw_on_query_failure'=>true,
    'force_compile'=>false,
];
$phad = \Phad::main($options);

$phad->filters['markdown'] = function($v){return 'pretend-this-is-markdown:<p>'.$v.'</p>';};

$phad->integration->setup_liaison_routes($lia);
$phad->integration->setup_liaison_route($lia, '/sitemap.xml', $phad->sitemap_dir.'/sitemap.xml');
// $custom_access = require(__DIR__.'/phad-access.php'); // returns an Access object

// $phad->access = $custom_access;

$phad->access_handlers['main_msg'] = 
    function($ItemInfo){
        echo "This is my custom deletion response. I don't care if deletion succeeded. Id was ".$ItemInfo->args['id'];
        return false;
    };

$phad->access_handlers['never_allow'] = 
    function($ItemInfo){
        return false;
    }
;

$phad->access_handlers['can_submit'] = 
    function($ItemInfo, $ItemRow){
        if (isset($_GET['deny_access'])&&$_GET['deny_access']=='true')return false;
        return true;
    }
;

$phad->access_handlers['permit_me'] = 
    function($data_node, $ItemInfo){
        if ($_GET['permit_me']=='true')return true;
        return false;
    };

$phad->handlers['user_has_role'] = 
function(string $role){
    if (isset($_GET['user'])&&$role == $_GET['user'])return true;
    return false;
}
;

$phad->handlers['can_read_row'] = 
    function(array $ItemRow,object $ItemInfo,string $ItemName){
        if (!isset($_GET['title']))return true;

        if ($ItemRow['title'] == $_GET['title']){
            return true;
        }

        return false;
    };


