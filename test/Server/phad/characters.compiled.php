<?php 
if (($phad_block??null)===\Phad\Blocks::SITEMAP_META){
    return array (
);
}
?><?php 
if ($phad_block===\Phad\Blocks::ROUTE_META){
    return array (
  0 => 
  array (
    'pattern' => '/characters/',
  ),
);
}
?>

<?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */






## becomes $BlogInfo
$CharacterInfo = (object)[
    'name'=> 'Character', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> array (
  0 => 'view',
  1 => 'data',
), //rawdata generally json. could be sql or csv though
    'type'=>'view', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => array (
  0 => 
  array (
    'sql' => 'SELECT character.*, species.name AS species FROM character AS Character JOIN species ON character.species = species.id',
    'type' => 'node',
  ),
  1 => 
  array (
    'type' => 'default',
  ),
), // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    
    
    
    
];



$phad->item_initialized($CharacterInfo);





if ($phad_block==\Phad\Blocks::ITEM_META){
    return $CharacterInfo;
}

foreach ($CharacterInfo->data_nodes as $_index=>&$_node){
    $_node['index'] = $_index;
    if (isset($args[':data'])){
        if (!isset($_node['name'])||$_node['name']!=$args[':data']){
            continue;
        }
    }
    if (isset($_node['if'])){
        //@todo templatize p-data 'if' attribute instead of using `eval()`
        $eval_code = 'return ('.$_node['if'].');';
        $result = eval($eval_code);
        if (!$result){
            $_node['response_code'] = 403;
            $CharacterInfo->response_code = 403;
            continue;
        }
    }
    if (!$phad->can_read_data($_node, $CharacterInfo)){
        $CharacterInfo->response_code = 403;
        $_node['response_code'] = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    $CharacterInfo->rows = $phad->read_data($_node, $CharacterInfo);
    if (count($CharacterInfo->rows)==0){
        $CharacterInfo->response_code = 404;
        $_node['response_code'] = 404;
        continue;
    }
    $CharacterInfo->response_code = 200;
    $_node['response_code'] = 200;
    $CharacterInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows($CharacterInfo);
}




if (count($CharacterInfo->rows)===0){
    $phad->no_rows_loaded($CharacterInfo);
}

foreach ($CharacterInfo->rows as $RowIndex_Character => $CharacterRow ): 
    $Character = $phad->object_from_row($CharacterRow, $CharacterInfo);
    

    
    ?><div>
    
    <h2><?=$Character->name?></h2>
    <p>Level: <?=$Character->level?></p>
    <p>Species: <?=$Character->species?></p>
</div><?php
endforeach;

?>

