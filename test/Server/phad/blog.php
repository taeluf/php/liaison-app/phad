<route pattern="/blog/{slug}/"></route>

<div item="Blog" >
    <p-data where="Blog.slug LIKE :slug"></p-data>
    <h1 prop="title"></h1>
    <x-prop prop="body" filter="markdown"></x-prop>
</div>
