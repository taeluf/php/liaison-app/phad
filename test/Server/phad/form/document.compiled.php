<?php 
if (($phad_block??null)===\Phad\Blocks::SITEMAP_META){
    return array (
);
}
?><?php 
if ($phad_block===\Phad\Blocks::ROUTE_META){
    return array (
  0 => 
  array (
    'pattern' => '/document/make/',
  ),
);
}
?>


<?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */






## becomes $BlogInfo
$DocumentInfo = (object)[
    'name'=> 'Document', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> array (
  0 => 'form',
  1 => 'create',
  2 => 'update',
), //rawdata generally json. could be sql or csv though
    'type'=>'form', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => array (
  0 => 
  array (
    'type' => 'default',
  ),
), // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    'candelete' => false,
    'diddelete' => false,
    'cansubmit' => false,
    'target' => '/document-list/',
];


$DocumentInfo->properties = 
    array (
  'title' => 
  array (
    'type' => 'text',
    'maxlength' => '75',
    'tagName' => 'input',
  ),
  'doc' => 
  array (
    'type' => 'file',
    'tagName' => 'input',
  ),
  'file_name' => 
  array (
    'type' => 'backend',
    'tagName' => 'input',
  ),
  'stored_name' => 
  array (
    'type' => 'backend',
    'tagName' => 'input',
  ),
  'id' => 
  array (
    'tagName' => 'input',
    'type' => 'hidden',
  ),
);



$phad->item_initialized($DocumentInfo);



if ($phad_block == \Phad\Blocks::FORM_DELETE){
    if ($phad->can_delete($DocumentInfo)){
        
        $did_delete = $phad->delete($DocumentInfo);
        if ($did_delete){
            
            $phad->did_delete($DocumentInfo);
            return;
        }
    }
}



if ($phad_block==\Phad\Blocks::ITEM_META){
    return $DocumentInfo;
}

foreach ($DocumentInfo->data_nodes as $_index=>&$_node){
    $_node['index'] = $_index;
    if (isset($args[':data'])){
        if (!isset($_node['name'])||$_node['name']!=$args[':data']){
            continue;
        }
    }
    if (isset($_node['if'])){
        //@todo templatize p-data 'if' attribute instead of using `eval()`
        $eval_code = 'return ('.$_node['if'].');';
        $result = eval($eval_code);
        if (!$result){
            $_node['response_code'] = 403;
            $DocumentInfo->response_code = 403;
            continue;
        }
    }
    if (!$phad->can_read_data($_node, $DocumentInfo)){
        $DocumentInfo->response_code = 403;
        $_node['response_code'] = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    $DocumentInfo->rows = $phad->read_data($_node, $DocumentInfo);
    if (count($DocumentInfo->rows)==0){
        $DocumentInfo->response_code = 404;
        $_node['response_code'] = 404;
        continue;
    }
    $DocumentInfo->response_code = 200;
    $_node['response_code'] = 200;
    $DocumentInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows($DocumentInfo);
}




if (count($DocumentInfo->rows)===0){
    $phad->no_rows_loaded($DocumentInfo);
}

foreach ($DocumentInfo->rows as $RowIndex_Document => $DocumentRow ): 
    $Document = $phad->object_from_row($DocumentRow, $DocumentInfo);
    
        if ($DocumentInfo->mode == \Phad\Blocks::FORM_SUBMIT){
            // <onsubmit> code goes here. Set `$DocumentInfo->mode = null` stop submission & display the form.
            ?><?php
        
                $DocumentRow['file_name'] = $_FILES['doc']['name'];
                $DocumentRow['stored_name'] = \Phad\PDOSubmitter::uploadFile($_FILES['doc'], 
                    dirname(__DIR__, 2).'/files-uploaded/',
                    ['txt']
                );
            ?><?php
            if ($DocumentInfo->mode==\Phad\Blocks::FORM_SUBMIT){
                if ($phad->can_submit($DocumentInfo, $DocumentRow)
                    &&$phad->submit($DocumentInfo, $DocumentRow)){
                    
                    $phad->redirect($DocumentInfo,$DocumentRow);
                    return;
                } else {
                    
                }
            }
            $args['DocumentSubmitErrorsList'] = $DocumentInfo->submit_errors;

            //  = $DocumentInfo->
            // print_r($args);
        }
    

    
    ?><form enctype="multipart/form-data" action="" method="POST">
    
    <input type="text" name="title" maxlength="75" value="<?=$Document->title?>">
    <input type="file" name="doc">

    
    
</form><?php
endforeach;

?>

