<route pattern="/document/make/"></route>


<form item="Document" target="/document-list/">
    <onsubmit><?php

        $DocumentRow['file_name'] = $_FILES['doc']['name'];
        $DocumentRow['stored_name'] = \Phad\PDOSubmitter::uploadFile($_FILES['doc'], 
            dirname(__DIR__, 2).'/files-uploaded/',
            ['txt']
        );
    ?></onsubmit>
    <input type="text" name="title" maxlength="75" />
    <input type="file" name="doc" />

    <input type="backend" name="file_name" />
    <input type="backend" name="stored_name" />
</form>
