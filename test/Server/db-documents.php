<?php

$lildb->create('document',
    [
        'id' => 'integer PRIMARY KEY AUTOINCREMENT',
        'title' => 'varchar(100)',
        'file_name' => 'varchar(250)',
        'stored_name' => 'varchar(250)',
    ]
);
//
// $lildb->insert('document',
    // [
        // 'id'=>1,
        // 'name'=>'use-of-force-report.txt',
        // 'stored_name' => 'use-of-force-report.txt'
    // ]
// );
