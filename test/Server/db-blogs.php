<?php

$lildb->create('blog',
    [
        'id' => 'integer PRIMARY KEY AUTOINCREMENT',
        'title' => 'varchar(100)',
        'slug' => 'varchar(100)',
        'body' => 'text',
    ]
);

$lildb->insert('blog',
    [
        'id'=>1,
        'title'=>'I like bears a lot',
        'body'=> 'Like seriously, I really love bears. They\'re so cute.'
    ]
);


$lildb->insert('blog',
    ['title'=>'Not Lorem Ipsum',
    'body'=> 'I could use lorem ipsum like most people do, but that\'s no fun.'
    ]
);

$lildb->insert('blog',
    ['title'=>'Baby Fires',
    'body'=> 'Is that "babies on fire" or "fires as small as babies"? The first one is probably a war crime. The second one is a weird way to describe a fire.'
    ]
);
