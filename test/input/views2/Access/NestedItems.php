<div item="Blog">
    <p-data name="type" where="Blog.type LIKE :type" limit="0,3"></p-data>
    <h1 prop="title"></h1>
    <p prop="description"></p>
    <div item="Blog">
        <p-data name="nottype" where="Blog.type NOT LIKE :type" limit="0,2"></p-data>
        <h2 prop="title"></h2>
        <span prop="description"></span>
    </div>
</div>
