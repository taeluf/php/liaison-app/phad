<?php 
     if ($phad_block===\Phad\Blocks::ROUTE_META){
         return array (
);
     }
    ?><?php 
     if (($phad_block??null)===\Phad\Blocks::SITEMAP_META){
         return array (
);
     }
    ?><?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */






## becomes $BlogInfo
$BlogInfo = (object)[
    'name'=> 'Blog', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> array (
  0 => 'form',
  1 => 'create',
  2 => 'update',
), //rawdata generally json. could be sql or csv though
    'type'=>'form', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => array (
  0 => 
  array (
    'type' => 'default',
  ),
), // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    'candelete' => true,
    
];


$BlogInfo->properties = 
    array (
  'id' => 
  array (
    'tagName' => 'input',
    'type' => 'hidden',
  ),
);



$phad->item_initialized($BlogInfo);



if ($phad_block == \Phad\Blocks::FORM_DELETE){
    if ($phad->can_delete($BlogInfo)){
        $did_delete = $phad->delete($BlogInfo);
        if ($did_delete){
            $phad->did_delete($BlogInfo);
            return;
        }
    }
}



if ($phad_block==\Phad\Blocks::ITEM_META){
    return $BlogInfo;
}

foreach ($BlogInfo->data_nodes as $_index=>&$_node){
    $_node['index'] = $_index;
    if (!$phad->can_read_data($_node, $BlogInfo)){
        $BlogInfo->response_code = 403;
        $_node['response_code'] = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    $BlogInfo->rows = $phad->read_data($_node, $BlogInfo);
    if (count($BlogInfo->rows)==0){
        $BlogInfo->response_code = 404;
        $_node['response_code'] = 404;
        continue;
    }
    $BlogInfo->response_code = 200;
    $_node['response_code'] = 200;
    $BlogInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows($BlogInfo);
}



foreach ($BlogInfo->rows as $RowIndex_Blog => $BlogRow ): 
    $Blog = $phad->object_from_row($BlogRow, $BlogInfo);
    
        if ($BlogInfo->mode == \Phad\Blocks::FORM_SUBMIT){
            // <onsubmit> code goes here. Set `$BlogInfo->mode = null` stop submission & display the form.
            
            if ($BlogInfo->mode==\Phad\Blocks::FORM_SUBMIT){
                if ($phad->submit($BlogInfo, $BlogRow)){
                    
                    $phad->redirect($BlogInfo,$BlogRow);
                    return;
                } else {
                    
                }
            }
            $args['BlogSubmitErrorsList'] = $BlogInfo->submit_errors;

            //  = $BlogInfo->
            // print_r($args);
        }
    

    
    ?><form action="" method="POST">
    <input type="text" prop="title">
</form><?php
endforeach;

?>

