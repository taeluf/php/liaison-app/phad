<form item="Blog">
    <on s=200><?php
        $BlogItem->properties['slug'] = ['type'=>'text', 'minlength'=>4, 'maxlength'=>150];
    ?></on>
    <input type="text" name="title" maxlength="75">
    <textarea name="body" maxlength="2000" minlength="50"></textarea>
</form>
