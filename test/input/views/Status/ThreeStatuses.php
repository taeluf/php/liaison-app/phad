<route pattern="/access/"></route>

<h2>Before Item Node</h2>
<div item="Blog" >
    <p-data role="admin" limit="1">
        <on s=404>Blogs Not Found</on>
        <on s=200>Access Granted</on>
        <on s=500>Server Error</on>
        <on s=403>User does not have admin role. User has role <?=$phad->user->role;?></on>
    </p-data>
    <!-- access-granted -->
    <h1 prop="title"></h1>
    <p prop="description"></p>
</div>
<h2>After Item Node</h2>
