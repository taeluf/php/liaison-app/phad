<route pattern="/blog/{slug}"> 
    <sitemap 
        sql="SELECT slug FROM blog" 
        handler="namespace:blogSitemapHandler"
        priority=0.8 
        last_mod=1354
        changefreq=daily
    >
    </sitemap>
</route>
