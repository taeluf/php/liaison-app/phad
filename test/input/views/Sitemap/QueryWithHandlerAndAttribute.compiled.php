<?php 
if ($phad_block===\Phad\Blocks::ROUTE_META){
    return array (
  0 => 
  array (
    'pattern' => '/blog/{slug}',
  ),
);
}
?><?php 
if (($phad_block??null)===\Phad\Blocks::SITEMAP_META){
    return array (
  '/blog/{slug}' => 
  array (
    'sql' => 'SELECT slug, last_mod, search_priority/100.00 AS priority FROM blog',
    'handler' => 'namespace:blogSitemapHandler',
    'priority' => '0.8',
    'pattern' => '/blog/{slug}',
  ),
);
}
?>
