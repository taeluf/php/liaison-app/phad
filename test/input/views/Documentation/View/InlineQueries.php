<div item="Blog">
    <p-data where="Blog.type LIKE :type">
        <on s=404><p><?=$type?> blogs not found</p></on>
    </p-data>
    <p-data where="Blog.type NOT LIKE :type" orderby="title ASC" limit="0,5">
        <on s=200><p>non-<?=$type?> blogs</p></on>
    </p-data>
    <h1 prop="title"></h1>
    <p prop="description"></p>
</div>
