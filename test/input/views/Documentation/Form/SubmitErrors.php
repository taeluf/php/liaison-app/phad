<form item="Blog" target="/blog/{id}/">
    When there's a submission failure, the error node is replaced with an error message. Otherwise, its just removed.
    <div class="my-error-class"><error></error></div>
    <input type="text" name="title" maxlength="75">
    <textarea name="body" maxlength="2000" minlength="50"></textarea>
</form>
