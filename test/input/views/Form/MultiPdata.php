<form item="Blog" target="/blog/{title}/">
    <p-data where="blog.title LIKE :title"></p-data>
    <p-data where="blog.body LIKE :body"></p-data>
    <p-data where="blog.id = :id"></p-data>
    <p-data type="default"></p-data>
    <input type="text" name="title" maxlength="75">
    <textarea name="body" maxlength="2000" minlength="50"></textarea>
</form>
