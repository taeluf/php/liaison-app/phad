<?php

namespace Phad;

require_once(__DIR__.'/TestHelper.php');

class Tester extends \Tlf\Tester {

    protected $pdo = '';
    public $phad;

    public function bootstrap(){
        require(__DIR__.'/bootstrap-server.php');
    }

    public function helper($initCompo = true){
        return new TestHelper($initCompo);
    }
    //
    // protected function has_node($textToTest,$tagName, $content){
    //     return $this->str_contains($textToTest,"<$tagName>$content</$tagName>");
    // }
    // protected function not_has_node($textToTest,$tagName, $content){
    //     return $this->str_not_contains($textToTest,"<$tagName>$content</$tagName>");
    // }

    protected function viewSrc($viewName){
        $viewPath = $this->file('test/input/views/').$viewName.'.php';
        if (file_exists($viewPath))return file_get_contents($viewPath);

        throw new \Exception("View '$viewName' does not exist at '$viewPath'");
    }

    protected function item($viewName, $args=[]){
        $this->phad = $this->phad ?? $this->phad();
        return $this->phad->item($viewName, $args);
    }

    protected function makeBlogTable(&$ListOfBlogs){
        $this->itemList($ListOfBlogs);
        $pdo = $this->pdo;
        $pdo->exec("DROP TABLE IF EXISTS blog");
        $pdo->exec("CREATE TABLE blog(title VARCHAR(254), description VARCHAR(1024), type VARCHAR(254))");
        $stmt = $pdo->prepare("INSERT INTO blog (title, description, type) VALUES(:title, :description, :type)");
        foreach ($ListOfBlogs as $OneBlog){
            $OneBlog = (array)$OneBlog;
            $stmt->execute($OneBlog);
        }
        return $ListOfBlogs;
    }

    protected function itemList(&$BlogList){
        $BlogList = [
            (object)[
                'title'=>'Cats',
                'description'=>'Cats are furry and cute and maybe a little evil',
                'type'=>'pet',
            ],
            (object)[
                'title'=>'Dogs',
                'description'=>'Dogs are furry and cute and maybe a little dumb',
                'type'=>'pet',
            ],
            (object)[
                'title'=>'Bears',
                'description'=>'Bears are the best, but they aren\'t trained for cuddles :(',
                'type'=>'wild',
            ]
        ];
    }

    public function rows(&$Blog = null, &$Song = null){
        $Blog = [
            'title'=>'What about wind turbine waste?',
            'description'=>'Its a real problem, and we should look for ways to solve it while still providing sustainable energy.',
            'type'=> 'static-post',
        ];

        $Song = [
            'title'=>'Losing Interest',
            'description'=> 'An absolutely lovely lofi song I found recently. Did you even know lo-fi is fantastic? I didn\'t... til recently'
        ];

        return ['Blog'=>$Blog, 'Song'=>$Song];
    }

}
