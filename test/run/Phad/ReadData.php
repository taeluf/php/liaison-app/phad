<?php

namespace Phad\Test\Phad;

class ReadData extends \Phad\Tester {

    /** 
     * Execute `$phad->read_data()` and test the results
     *
     * @param $msg a message to print in the test results
     * @param $expect the data that should be returned by the query
     * @param $node an array of query info as would be extracted from a data node
     * @param $args array of args to use in the query (they're added to the ItemInfo, then `Phad->read_data()` uses args, depending on the query & the item.
     * @param $pdo a pdo instance, if you're running an sql query
     */
    public function read_data(string $msg, array $expect, array $node, array $args=[], ?\PDO $pdo=null){
        $ItemInfo = (object)[
            'name'=>'Blog',
            'args'=>[],
            'type'=>$args['type'] ?? 'view',
            'mode'=>\Phad\Blocks::VIEW,
        ];
        if (is_array($args))$ItemInfo->args = $args;

        $phad2 = new \Phad();
        $phad2->pdo = $pdo;
        $data = $phad2->read_data($node, $ItemInfo);
        // print_r($data);

        if ($expect==$data){
            $this->handleDidPass(true, false);
            echo "\nPass: ".$msg;
        } else {
            $this->handleDidPass(false, false);
            echo "\nFail: ".$msg;
        }

        return $data;
    }

    /** create `blog` table and insert rows */
    public function insert(array $rows){
        $ldb = \Tlf\LilDb::sqlite();
        $ldb->create('blog',
            [
                'id'=>'integer',
                'title'=>'varchar(254)',
            'body'=>'varchar(500)',
            ]
        );

        $ldb->insertAll('blog', $rows);
        return $ldb->pdo;
    }

    /**
     * @test running queries & getting resultant rows
     * @test `Phad->read_data()` using static queries (no `:params` to bind)
     * @test `Phad->read_data()` using dynamic queries (with `:params` to bind)
     */
    public function testReadDataQuery(){
        $ItemInfo = [
            'name'=>'Blog',
            'args'=>null,
        ];

        $ldb_main = $this->insert(
            $main_rows = [
                ['id'=>0, 'title'=>'qtitle0', 'body'=>'qbody0'],
                ['id'=>1, 'title'=>'qtitle1', 'body'=>'qbody1'],
                ['id'=>2, 'title'=>'qtitle2', 'body'=>'qbody2'],
                ['id'=>3, 'title'=>'qtitle3', 'body'=>'qbody3'],
                ['id'=>4, 'title'=>'qtitle4', 'body'=>'qbody4'],
            ]
        );

        $query_rows = $this->read_data(
            'Default Query, one item',
            $expect = [
                ['id'=>0, 'title'=>'query_title', 'body'=>'query_body',]
            ],
            ['type'=>'default'],
            [],
            $this->insert($expect),
        );

        $query_rows = $this->read_data(
            'Default Query, multiple items',
            $main_rows,
            ['type'=>'default'],
            [],
            $ldb_main,
        );


        $query_rows = $this->read_data(
            'Query with limit',
            array_slice($main_rows,0,3),
            ['type'=>'custom', 'limit'=>'3'],
            [],
            $ldb_main,
        );

        $query_rows = $this->read_data(
            'Query with where and :param',
            array_slice($main_rows,0,3),
            ['type'=>'custom', 'where'=>'id <= :max'],
            ['max'=>2],
            $ldb_main,
        );

        $query_rows = $this->read_data(
            'Query with custom sql',
            array_slice($main_rows,3),
            ['type'=>'custom', 'sql'=>'select * from blog where id > :min'],
            ['min'=>2],
            $ldb_main,
        );
        // print_r($query_rows);
    }

    /**
     * @test `Phad->read_data()` using Blog, BlogList, and no data
     */
    public function testReadDataNoQuery(){
        $ItemInfo = [
            'name'=>'Blog',
            'args'=>null,
        ];


        $this->read_data(
            'One Row passed in ',
            [ // expect
                ['title'=>'title1','body'=>'body1'],
            ],
            ['type'=>'default'], // data node
            [ // $args
                'Blog'=>['title'=>'title1','body'=>'body1'],
            ],
        );

        $this->read_data(
            'List of Rows passed in',
            [ // expect
                ['title'=>'title1','body'=>'body1'],
                ['title'=>'title2','body'=>'body2'],
            ],
            ['type'=>'default'], // data node
            [ // $args
                'BlogList'=>[
                    ['title'=>'title1','body'=>'body1'],
                    ['title'=>'title2','body'=>'body2'],
                ]
            ],
        );

        $this->read_data(
            'No Rows, no data to query, no data',
            [],
            ['type'=>'default'],
            []
        );

        $this->read_data(
            'Form type, no data, black hole',
            [['_object'=>'Phad\\BlackHole']],
            ['type'=>'default'],
            ['type'=>'form'],
        );
    }


}
