<?php

namespace Phad\Test\Phad;

class Submit extends \Phad\Tester {

    /**
     * @param $msg for printing in test results
     * @param $require_title `true/false` to add `required` to the `title` property
     * @param $expect_success `true/false` whether the submission should insert data or not
     * @param $row the data to insert (title, body, optional id)
     * @param $expect array of expectations
     */
    public function exec_submit_test(string $msg, bool $require_title, bool $expect_success, array $row, array $expect){
        $phad = new \Phad();
        $ldb = \Tlf\LilDb::sqlite();
        $phad->pdo = $ldb->pdo;
        $ldb->create('blog',
            [
                'id'=>'integer PRIMARY KEY AUTOINCREMENT',
                'title'=>'varchar(254)',
                'body'=>'varchar(500)',
            ]
        );

        
        $ItemInfo = (object)[
            'name'=>'Blog',
            'submit_errors'=>[],
        ];

        /** the form inputs */
        $ItemInfo->properties = [
            'title' => 
            [
                'type' => 'text',
                'maxlength' => '75',
                'tagName' => 'input',
            ],
            'body' => 
            [
                'maxlength' => '2000',
                'minlength' => '50',
                'tagName' => 'textarea',
            ],
            'id'=>
            [
                'tagName'=>'input',
                'type'=>'hidden',
            ]
        ];
        if ($require_title)$ItemInfo->properties['title']['required'] = '';

        // print_r($row);
        $result = $phad->submit($ItemInfo,$row);
        // var_dump($result);
        // exit;

        $rows = $ldb->select('blog');
        // print_r($rows);

        // echo "\"

        // print_r($ItemInfo->submit_errors);
        // exit;

        if ($result==true&&count($rows)===1 && $expect_success === true){
            echo "\n+ Pass $msg: Data was submitted";
            $this->handleDidPass(true,false);
        } else if ($result===false&&$expect_success===false&&count($rows)===0){
            echo "\n+ Pass $msg: Data did not submit, as expected";
            $this->handleDidPass(true,false);
        } else if ($result==true && count($rows)==1 && $expect_success == false){
            echo "\n- Fail $msg: Data submitted, but was not supposed to.";
            $this->handleDidPass(false,false);
        } else if ($expect_success==true && $result == false && count($rows)==0){
            echo "\n- Fail $msg: Data did not submit, but was supposed to.";
            $this->handleDidPass(false,false);
        } else {
            echo "\n- Fail $msg: Test Failed, not sure why.";
            $this->handleDidPass(false,false);
        }

        $expect_count = count($expect['errors']);
        $actual_count = count($ItemInfo->submit_errors);
        // $
        if ($ItemInfo->submit_errors == $expect['errors']){
            echo "\n    + Pass errors: Expected $expect_count, got $actual_count";
            $this->handleDidPass(true,false);
        } else {
            echo "\n    + Fail errors: Expected $expect_count, got $actual_count";
            $this->handleDidPass(false,false);
            echo "\n      Errors (expected): ".$this->comparisonOutput($expect['errors']).'';
            echo "\n      Errors (actual): ".$this->comparisonOutput($ItemInfo->submit_errors).'';
        }

    }

    
    /**
     * @test submitting a row with empty data
     * @test submitting a row that's missing a required column
     * @test submitting NO data & getting sql error
     */
    public function testSubmit(){

        /** ..._test(msg, require title or not, expect success or not, row to insert, expected errors) */
        $this->exec_submit_test('Empty Title was submitted', false, true, ['title'=>''],
            [
                'errors'=>[],
            ],
        );

        $this->exec_submit_test('Empty title was required', true, false, ['title'=>''],
            [
                'errors'=>[
                    ['msg'=>"'title' failed validation for 'required:true'"],
                ],
            ],
        );


        $this->exec_submit_test('No Columns, sql error', false, false, [],
            [
                'errors'=>[
                    ['msg'=>"Submission was valid, but there was an unknown technical error. Error code 'HY000'"],
                ],
            ],
        );


        // what do i expect when i submit no data?
            // `false` to indicate "did not submit"
            // `$ItemInfo->errors` should maybe ... list an error YES YES YES 
            // `$phad->errors` maybe should have errors

    }
}
