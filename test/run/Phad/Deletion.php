<?php

namespace Phad\Test\Phad;

class Submission extends \Phad\Tester {


    public function testDeleteItemSS(){
        $lildb = \Tlf\LilDb::sqlite();
        $pdo = $lildb->pdo();
        $phad = $this->phad();
        $phad->pdo = $pdo;
        $lildb->create('blog',['id'=>'integer', 'title'=>'varchar(90)']);
        $lildb->insert('blog',['id'=>1,'title'=>'title 1']);
        $lildb->insert('blog',['id'=>2,'title'=>'title 2']);
        $lildb->insert('blog',['id'=>3,'title'=>'title 3']);

        $args = ['id'=>2];
        $mode = 'delete';
        $BlogItem = (object)[
            'accessList'=>[], 
            'args'=>$args, 
            'list'=>[], 
            'name'=>'Blog', 
            'accessStatus'=>200, 
            'phad_mode'=>$mode, 
            'item_type'=>'form'
        ];

        $phad->delete($BlogItem);


        $blogs = $lildb->select('blog');
        $this->compare(
            [ ['id'=>1,'title'=>'title 1'],
              ['id'=>3,'title'=>'title 3'],
            ],
            $blogs
        );
    }

    public function phad($idk=null){
        $phad = new \Phad();
        $phad->exit_on_redirect = false;
        $phad->force_compile = true;
        $phad->item_dir = $this->file('test/input/views/');
        return $phad;
    }

}
