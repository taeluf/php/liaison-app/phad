<?php

namespace Phad\Test\Phad;

class Routes extends \Phad\Tester {

    public function testCacheAllRoutes(){
        $cache_file = $this->file('test/input/cache/phad-routes.php');
        if (file_exists($cache_file)){
            unlink($cache_file);
        }

        $phad = new \Phad();
        $phad->item_dir = $this->file('test/input/views2');
        $phad->cache_dir = $this->file('test/input/cache');

        $target = $phad->routes_from_cache();

        $views = $phad->get_all_items();
        $routes = $phad->routes_from_items($views, __DIR__.'/phad/');
        $expect = $routes;

        $target2 = $phad->routes_from_cache();

        $target3 = require($cache_file);

        $this->compare($expect, $target);
        $this->compare($expect, $target2);
        $this->compare($expect, $target3);
    }

    public function testGetAllRoutes(){
        $phad = new \Phad();
        $phad->force_compile = true;
        $phad->item_dir = $this->file('test/input/views2');
        $views = $phad->get_all_items();
        $routes = $phad->routes_from_items($views, __DIR__.'/phad/');
        // print_r($routes);

        $expect = [
            //two /blog/{slug} routes are defined, but the first one (commented out) is overwritten by the second
            '/route/no-items/' => 'Route/NoItems',
            '/route/still-no-items/' => 'Route/NoItems',
            '/blog/{title}/' => 'Route/SimpleBlog',
            # '/blog/{slug}' => 'Sitemap/QueryWithDeclaredSitemapAttributes', 
            '/blog/{slug}' => 'Sitemap/QueryWithHandlerAndAttribute', 
            '/access/' => 'Status/ThreeStatuses',
        ];

        $this->compare($expect, $routes);
    }

    public function testGetAllViews(){
        $phad = new \Phad();
        $phad->item_dir = $this->file('test/input/views2');
        $views = $phad->get_all_items();

        // this list was made just by dumping $views & comparing to what is on disk
        $expect = [
            'Access/AdjacentItems',
            'Access/ModificationFromParentItem',
            'Access/MultiAccess',
            'Access/NestedItems',
            'Access/OneAccess',
            'Form/BlogWithCategories',
            'Form/BlogWithOnSubmit',
            'Form/SimpleBlog',
            'Form/SimpleBlogNoTarget',
            'Form/SimpleBlogWithError',
            'Form/Candelete',
            'Other/AdjacentAndNestedItems',
            'Other/AdjacentItems',
            'Other/NoAccess',
            'Other/OneNestedItem',
            'Other/SimpleBlogWithFilter',
            'Route/NoItems',
            'Route/SimpleBlog',
            'Sitemap/QueryWithDeclaredSitemapAttributes',
            'Sitemap/QueryWithHandlerAndAttribute',
            'Status/ManyBranches',
            'Status/OnlyHas200',
            'Status/ThreeStatuses',
        ];
        sort($views);
        sort($expect);
        $this->compare_arrays($expect, $views);
    }
}
