<?php

namespace Phad\Test\Main;

/**
 * Less for actually testing & more for benchmarking
 */
class SpamControls extends \Phad\Tester {


    /**
     * This is just for benchmarking
     * the password_hash() was incredibly slow because the default cost is reasonably high.
     */
    public function testGenerateSpamControls(){
        $this->disable();
        $start = microtime(true);
        // $phad = $this->phad();
        $phad = new \Phad();

        $iters = 0;
        for ($i=0;$i<10;$i++){
            // $spam_controls = $phad->show_spam_control('test');

            // honey_form was really slow until reducing the bcrypt algo cost to 4
            $spam_controls = $phad->honey_form();
            

            $csrf_key = $phad->make_csrf_code();
            $iters++;
        }


        $end = microtime(true);
        $duration = $end - $start;

        echo "\n\n\n-----------\n\n";
        echo "\nTotal($iters loops): ".$duration;
        echo "\nAvg Per Loop: ". ($duration / $iters);
        echo "\n\n\n-----------\n\n";

    }

}
