<?php

namespace Phad\Test\Integration;

/**
 * Tests that the compiler correctly generates route data & that View class can return it
 */
class Routing extends \Phad\Tester {

    /** the template file contents */
    protected $template;

    public function prepare(){
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $this->pdo = new \PDO('sqlite::memory:');
        $this->template = file_get_contents($this->file('code/template/main.php'));
    }

    public function testRouteDataFromView2(){
        $phad = new \Phad();
        $phad->item_dir = $this->file('test/input/views/');
        $view = $phad->item('Route/NoItems');
        
        $routes = $view->routes();

        $this->compare(
            [['pattern'=>'/route/no-items/'], ['pattern'=>'/route/still-no-items/']],
            $routes
        );
    }

    public function testRouteDataFromView(){
        $phad = new \Phad();
        $phad->item_dir = $this->file('test/input/views/');
        $view = $phad->item('Route/SimpleBlog');
        $view->force_compile = true;
        
        $routes = $view->routes();

        $this->compare(
            [['pattern'=>'/blog/{title}/']],
            $routes
        );
    }

    public function testRouteDataExtractedFromTemplate(){
        $compiler = new \Phad\TemplateCompiler();
        $out = $compiler->compile($this->viewSrc('Route/SimpleBlog'), $this->template);
        $phad_block = \Phad\Blocks::ROUTE_META;

        $routes = eval('?>'.$out);

        $this->compare(
            [['pattern'=>'/blog/{title}/']],
            $routes
        );
    }

}
