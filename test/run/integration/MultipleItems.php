<?php

namespace Phad\Test\Integration;

/**
 * Tests involving multiple item nodes in a single view
 */
class MultipleItems extends \Phad\Tester {

    public $phad;

    protected function standardTest($view,$Blog=null){
        $this->rows($Blog, $Song);

        $this->test('Both items are cleaned up');
            $this->str_not_contains($view, ['item=','prop=']);

        $this->test('Blog content');
            $this->str_contains($view, '<h1>'.$Blog['title'].'</h1>');
            $this->str_contains($view, '<p>'.$Blog['description'].'</p>');

        $this->test('Song content');
            $this->str_contains($view, '<h1>'.$Song['title'].'</h1>');
            $this->str_contains($view, '<p>'.$Song['description'].'</p>');
    }

    public function testAdjacentItemsWithNestedItems(){
        // $compo = $this->compo($lia);
        $items = $this->rows();

        $items['Page'] = ['title'=>'This is a page'];
        $items['Writer'] = ['name'=> 'A writer'];
        $items['Artist'] = ['name'=> 'An artist'];
        $items['Footer'] = ['copyright' => '2021 Taeluf'];

        $view = $this->item('Other/AdjacentAndNestedItems', $items);
        // $view = $compo->item('Blog/AdjacentWithNested', $items);
        $this->standardTest($view);

        $this->test('Writer');
            $this->str_contains($view, '<h1>'.$items['Writer']['name'].'</h1>');

        $this->test('Artist');
            $this->str_contains($view, '<h1>'.$items['Artist']['name'].'</h1>');

        $this->test('Page');
            $this->str_contains($view, '<h1>'.$items['Page']['title'].'</h1>');

        $this->test('Footer');
            $this->str_contains($view, '<p>'.$items['Footer']['copyright'].'</p>');
    }

    public function testOneNestedItem(){
        $view = $this->item('Other/OneNestedItem', $this->rows());
        $this->standardTest($view);
    }

    public function testAdjacentItems(){
        $phad = $this->phad();
        $this->rows($Blog, $Song);

        $view = $phad->item('Other/AdjacentItems',['Blog'=>$Blog, 'Song'=>$Song]);

        $this->standardTest($view);
    }

    public function phad($idk=null){
        $phad = new \Phad();
        $phad->exit_on_redirect = false;
        $phad->force_compile = true;
        $phad->item_dir = $this->file('test/input/views/');
        return $phad;
    }

}
