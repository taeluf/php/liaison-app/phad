<?php

namespace Phad\Test\Integration;


class Component extends \Phad\Tester {

    public function prepare(){
        $this->pdo = new \PDO('sqlite::memory:');
    }

    public function testCustomFilter(){
        // $this->makeBlogTable($ListOfBlogs);
        // $Bear = (object)$ListOfBlogs[2];
        // $compo = $this->compo($lia, $package);
        // $lia->api('phad:filter.add', 'namespace:filter_name', 'strtoupper');

        $phad = $this->phad();
        $phad->pdo = $this->pdo;
        $phad->filters['namespace:filter_name'] =  'strtoupper';

        $view = $phad->item('Other/SimpleBlogWithFilter', ['Blog'=>($Blog=['title'=>'Blog Post', 'description'=>'This is uppercase after applying the filter'])]);

        $this->str_contains($view, '<p>'.strtoupper($Blog['description']).'</p>');

        echo $view;
    }

    public function testInlineModificationsWithNestedItems(){
        $this->makeBlogTable($ListOfBlogs);
        $Bear = (object)$ListOfBlogs[2];

        $phad = $this->phad();
        $phad->pdo = $this->pdo;
        $view = $phad->item('Access/ModificationFromParentItem', ['type'=>'pet']);

        $this->str_contains($view, '<div>');
        $this->str_contains($view, '</div>');
        $this->str_not_contains($view, ['item=','prop=']);
        foreach ($ListOfBlogs as $Blog){
            $Blog = (object)$Blog;
            if ($Blog==$Bear){
                $this->str_not_contains($view, '<h1>'.$Blog->title.'</h1>');
                $this->str_not_contains($view, '<p>'.$Blog->description.'</p>');
                $this->str_not_contains($view, '<h2>'.$Blog->title.'</h2>');
                $this->str_not_contains($view, '<span>'.$Blog->description.'</span>');
            } else {
                $this->str_contains($view, '<h1>'.$Blog->title.'</h1>');
                $this->str_contains($view, '<p>'.$Blog->description.'</p>');
                $this->str_contains($view, '<h2>'.$Blog->title.'</h2>');
                $this->str_contains($view, '<span>'.$Blog->description.'</span>');
            }
        }

        echo $view;
    }

    public function testQueryNestedItems(){
        $this->makeBlogTable($ListOfBlogs);

        $phad = $this->phad();
        $phad->pdo = $this->pdo;
        $view = $phad->item('Access/NestedItems', ['type'=>'pet']);
        

        $this->str_contains($view, '<div>');
        $this->str_contains($view, '</div>');
        $this->str_not_contains($view, ['item=','prop=']);
        foreach ($ListOfBlogs as $Blog){
            $Blog = (object)$Blog;
            if ($Blog->type=='pet'){
                $this->str_contains($view, '<h1>'.$Blog->title.'</h1>');
                $this->str_contains($view, '<p>'.$Blog->description.'</p>');
            } else {
                $this->str_contains($view, '<h2>'.$Blog->title.'</h2>');
                $this->str_contains($view, '<span>'.$Blog->description.'</span>');
            }
        }

        echo $view;
    }

    public function testQueryAdjacentItems(){
        $this->makeBlogTable($ListOfBlogs);

        $phad = $this->phad();
        $phad->pdo = $this->pdo;
        $view = $phad->item('Access/AdjacentItems', ['type'=>'pet']);
        

        $this->str_contains($view, '<div>');
        $this->str_contains($view, '</div>');
        $this->str_not_contains($view, ['item=','prop=']);
        foreach ($ListOfBlogs as $Blog){
            $Blog = (object)$Blog;
            if ($Blog->type=='pet'){
                $this->str_contains($view, '<h1>'.$Blog->title.'</h1>');
                $this->str_contains($view, '<p>'.$Blog->description.'</p>');
            } else {
                $this->str_contains($view, '<h2>'.$Blog->title.'</h2>');
                $this->str_contains($view, '<span>'.$Blog->description.'</span>');
            }
        }

        echo $view;
    }

    public function testNameFirstOfMultipleAccessNodesv2(){
        $this->makeBlogTable($ListOfBlogs);

        $phad = $this->phad();
        $phad->pdo = $this->pdo;
        $view = $phad->item('Access/MultiAccess', [':data'=>'type', 'type'=>'pet']);
        // todo:
            // in can_read_data (or can_read_node()??) check $args['data.name'] ... i guess
        
        $this->str_contains($view, '<div>');
        $this->str_contains($view, '</div>');
        $this->str_not_contains($view, ['item=','prop=']);
        foreach ($ListOfBlogs as $Blog){
            $Blog = (object)$Blog;
            if ($Blog->type=='pet'){
                $this->str_contains($view, '<h1>'.$Blog->title.'</h1>');
                $this->str_contains($view, '<p>'.$Blog->description.'</p>');
            } else {
                $this->str_not_contains($view, '<h1>'.$Blog->title.'</h1>');
                $this->str_not_contains($view, '<p>'.$Blog->description.'</p>');
            }
        }
    }
    public function testNameSecondOfMultipleAccessNodesv2(){
        $this->makeBlogTable($ListOfBlogs);

        $phad = $this->phad();
        $phad->pdo = $this->pdo;
        $view = $phad->item('Access/MultiAccess', [':data'=>'nottype', 'type'=>'pet']);
        // todo:
            // in can_read_data (or can_read_node()??) check $args['data.name'] ... i guess
        
        $this->str_contains($view, '<div>');
        $this->str_contains($view, '</div>');
        $this->str_not_contains($view, ['item=','prop=']);
        foreach ($ListOfBlogs as $Blog){
            $Blog = (object)$Blog;
            if ($Blog->type=='pet'){
                $this->str_not_contains($view, '<h1>'.$Blog->title.'</h1>');
                $this->str_not_contains($view, '<p>'.$Blog->description.'</p>');
            } else {
                $this->str_contains($view, '<h1>'.$Blog->title.'</h1>');
                $this->str_contains($view, '<p>'.$Blog->description.'</p>');
            }
        }
    }
    public function testNameFirstOfMultipleAccessNodes(){
        $this->makeBlogTable($ListOfBlogs);

        $phad = $this->phad();
        $phad->pdo = $this->pdo;
        $view = $phad->item('Access/MultiAccess', ['data.name'=>'type', 'type'=>'pet']);
        // todo:
            // in can_read_data (or can_read_node()??) check $args['data.name'] ... i guess
        
        $this->str_contains($view, '<div>');
        $this->str_contains($view, '</div>');
        $this->str_not_contains($view, ['item=','prop=']);
        foreach ($ListOfBlogs as $Blog){
            $Blog = (object)$Blog;
            if ($Blog->type=='pet'){
                $this->str_contains($view, '<h1>'.$Blog->title.'</h1>');
                $this->str_contains($view, '<p>'.$Blog->description.'</p>');
            } else {
                $this->str_not_contains($view, '<h1>'.$Blog->title.'</h1>');
                $this->str_not_contains($view, '<p>'.$Blog->description.'</p>');
            }
        }
    }
    public function testNameSecondOfMultipleAccessNodes(){
        $this->makeBlogTable($ListOfBlogs);

        $phad = $this->phad();
        $phad->pdo = $this->pdo;
        $view = $phad->item('Access/MultiAccess', ['data.name'=>'nottype', 'type'=>'pet']);
        // todo:
            // in can_read_data (or can_read_node()??) check $args['data.name'] ... i guess
        
        // echo $view;
        // exit;

        $this->str_contains($view, '<div>');
        $this->str_contains($view, '</div>');
        $this->str_not_contains($view, ['item=','prop=']);
        foreach ($ListOfBlogs as $Blog){
            $Blog = (object)$Blog;
            if ($Blog->type=='pet'){
                $this->str_not_contains($view, '<h1>'.$Blog->title.'</h1>');
                $this->str_not_contains($view, '<p>'.$Blog->description.'</p>');
            } else {
                $this->str_contains($view, '<h1>'.$Blog->title.'</h1>');
                $this->str_contains($view, '<p>'.$Blog->description.'</p>');
            }
        }
    }

    public function testQuerySecondOfMultipleAccessNodes(){
        $this->makeBlogTable($ListOfBlogs);

        $phad = $this->phad();
        $phad->pdo = $this->pdo;
        $view = $phad->item('Access/MultiAccess', ['type'=>'not-a-valid-type']);
        

        $this->str_contains($view, '<div>');
        $this->str_contains($view, '</div>');
        $this->str_not_contains($view, ['item=','prop=']);
        foreach ($ListOfBlogs as $Blog){
            $Blog = (object)$Blog;
            // if ($Blog->type!='pet')continue;
            $this->str_contains($view, '<h1>'.$Blog->title.'</h1>');
            $this->str_contains($view, '<p>'.$Blog->description.'</p>');
        }
    }
    public function testQueryingFirstOfMultipleAccesses(){
        $this->makeBlogTable($ListOfBlogs);

        $phad = $this->phad();
        $phad->pdo = $this->pdo;
        $view = $phad->item('Access/MultiAccess', ['type'=>'pet']);
        

        $this->str_contains($view, '<div>');
        $this->str_contains($view, '</div>');
        $this->str_not_contains($view, ['item=','prop=']);
        foreach ($ListOfBlogs as $Blog){
            $Blog = (object)$Blog;
            if ($Blog->type!='pet')continue;
            $this->str_contains($view, '<h1>'.$Blog->title.'</h1>');
            $this->str_contains($view, '<p>'.$Blog->description.'</p>');
        }
    }

    public function testPassingObjectToIgnoreAccessNode(){
        $this->rows($Blog);
        $view = $this->item('Access/OneAccess',['Blog'=>$Blog]);
        

        $this->str_not_contains($view, ['item=','prop=']);
        $this->str_contains($view, '<h1>'.$Blog['title'].'</h1>');
        $this->str_contains($view, '<p>'.$Blog['description'].'</p>');
    }

    public function testItemViewWithObject(){
        $this->rows($Blog);
        $view = $this->item('Other/NoAccess',['Blog'=>$Blog]);
        $this->str_not_contains($view, ['item=','prop=']);
        $this->str_contains($view, '<h1>'.$Blog['title'].'</h1>');
        $this->str_contains($view, '<p>'.$Blog['description'].'</p>');
    }



    public function phad($idk=null){
        $phad = new \Phad();
        $phad->exit_on_redirect = false;
        $phad->force_compile = true;
        $phad->item_dir = $this->file('test/input/views/');
        return $phad;
    }
    public function item($name, $args=[]){
        $phad = new \Phad();
        $args['phad'] = $phad;
        $item = new \Phad\Item($name, $this->file('test/input/views/'),$args);
        $item->force_compile = true;
        return $item;
    }
}
