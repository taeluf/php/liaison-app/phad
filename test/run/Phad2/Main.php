<?php

namespace Phad\Test\Phad2;


/**
 * This is a test class for developing some API wrapper classes, to make Phad easier to use
 */
class Main extends \Phad\Tester {

    /**
     * Testing a wrapper class that should provide a clean API for working with an item.
     */
    public function testWrapper(){
        $this->disable();
        return;

        $config = json_decode(file_get_contents(''));
        $phad = new \Phad();
        $phad->initialize($config);
        \Phad\Integration\Liaison::integrate($phad);
        $phad->enable_dev_mode();


        // below is what I mainly want to build
        $args = [];
        $item = $phad->item('some/item', $args);
        $data = $item->getData('which_pdata_node'); # probably require named nodes? idk
        // $children = $item->childItems; // this is likely far away
        // $siblings = $item->siblingItems; // likely far away
        $is_submission_valid = $item->validate_submission($data); // probably pass data by reference & modify it during the validation process?


        // or during an onsubmit
        // $item->setSubmission($_POST); // do this automatically, and use the item, instead of phad, to interface with the submission validation
        $item->remove_column('markdown');
        $item->add_column('html', ['maxlength'=>1000]);
        $item->saveSubmission($_POST); // by interacting with the data layer
        $item->getSubmissionErrors();


    }

    public function testEasierPhad(){
        $this->disable();
        return;

        // 1. get options, including configuered phad.class
        // 2. instantiate phad with those configs
        // 3. Initialize `$Phad->sitemap` & configure
        // 4. Initialize `$Phad->integration` & configure
        // 5. Setup initial methods, routes,
        //
        // What are all the parts?
        // - 3 directories (items, cache, sitemap)
        // - PDO (mysql connection)
        // - debug/devmode settings
        // - router
        // - sitemap. Configures from phad settings
        // - Integration. Also configured from phad setting
        // - Required Handlers (which should probably use an interface!)


        // So what do I want?
        // To instantiate phad from config & nothing else
        // I would not mind one additional method call to setup the Liaison integration.
        // And one optional method to enable debugging would probably be okay too
        // I don't want to instantiate my own PDO, but that would be okay, I guess.



        $dir = $app->dir;
        $pdo = $this->pdo ?? $app->lia->pdo ?? null;
        $debug = $this->debug;
        $route_prefix = $app->base_url;
        $options = [
            'item_dir'=>$dir.'/phad/',
            'cache_dir'=>$dir.'/cache/',
            'sitemap_dir'=>$dir.'/sitemap/',
            'pdo' => $pdo,// a pdo object
            'router' => $this->addon('lia:server.router'),
            'throw_on_query_failure'=>$debug,
            'force_compile'=>$debug,
        ];
        $phad_class = $app->config['phad.class'] ?? '\\Phad';
        // var_dump($phad_class);
        $phad = $phad_class::main($options);
        $app->phad = $phad;
        $phad->route_prefix = $route_prefix;
        $phad->global_phad_args['lia'] = $this;
        // $this->set('phad', $phad);
        $this->addMethod('phad', [$this, 'phad_item']);

        $phad->filters['markdown'] = [$this, 'markdown_to_html'];

        $phad->integration->setup_liaison_routes($this);
        $phad->integration->setup_liaison_route($this, '/sitemap.xml', $phad->sitemap_dir.'/sitemap.xml');

        $phad->handlers['user_has_role'] = 
        function(string $role){
            if (isset($this->user))return $this->user->has_role($role);
            // echo 'generic user has role';
            // exit;
            return true;
        }
        ;

        $phad->handlers['can_read_row'] = 
            function(array $ItemRow,object $ItemInfo,string $ItemName){
                return true;
            };

        if (isset($app->config['phad.handler']) && isset($this->user)){
            $handler = $app->config['phad.handler'];
            $phad->set_handler(new $handler($this->user));
            // var_dump($handler);
            // exit;
        }

        $this->phads[] = $phad;
        return $phad;
    }



    public function phad_main(array $options){

        $class = static::class;
        $phad = $custom_phad ?? new $class();

        $phad->configs = $options;
        foreach ($options as $k=>$v){
            $phad->$k = $v;
        }

        $phad->sitemap = new \Phad\SitemapBuilder($phad->sitemap_dir);
            $phad->sitemap->cache_dir = &$phad->cache_dir;
            $phad->sitemap->pdo = &$phad->pdo;
            $phad->sitemap->throw_on_query_failure = &$phad->throw_on_query_failure;
            $phad->sitemap->router = &$phad->router;
            $phad->sitemap->handlers = &$phad->sitemap_handlers;
        // $phad->filter = new \Phad\Filter();
        // $phad->access = new \Phad\Access();
        //     $phad->access->pdo = &$phad->pdo;
        //     $phad->access->throw_on_query_failure = &$phad->throw_on_query_failure;
        //     $phad->access->user = &$phad->user;
        // $phad->stack = new \Phad\Stack();
        // $phad->form = new \Phad\FormValidator();
        //     $phad->form->validators = &$phad->validators;
        //     $phad->form->throw_submission_error = &$phad->throw_submission_error;
        // $phad->submitter = new \Phad\PDOSubmitter();
            // $phad->submitter->pdo = &$phad->pdo;
            // $phad->submitter->lildb = new \Tlf\LilDb($phad->pdo);
        $phad->integration = new \Phad\Integration();
            $phad->integration->phad = $phad;
            $phad->integration->force_compile = &$phad->force_compile;

        return $phad;
    }
}
