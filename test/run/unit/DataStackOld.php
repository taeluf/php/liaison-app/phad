<?php

namespace Phad\Test\Unit;

/**
 * @note(feb 3, 2022) added 'Old' to the name because I'm not currently using the data stack. The tests are still here because I'm probably going to re-enable it
 */
class DataStackOld extends \Phad\Tester {

    /**
     * In theory, I should test the nested item stack as well, so if I add a 'SongList' nested in a 'BlogList' that is properly represented. However, my one-item-tests kind of ensure this. 
     *
     * After 'row started stack', the head is `$stack->data['BlogList'][0]`, which is an array of blog data. A `SongList` could be seemlessly added to this, the same way a 'BlogList' was added to the root data array.
     *
     * Since single-item tests already verify that the ending state (after itemListFinished) is identical to the starting state (before itemListStarted), I have high confidence that the state will be restored properly when the nested 'SongList' item is done with itemListFinished(). 
     *
     * Similarly, i have high confidence the SongList will be added with the correct structure
     *
     */
    public function testNestedItemStack(){
        $this->disable();
    }

    /**
     * In theory, I should test for adjacent items, but adjacent item needs the head & stack in an identical state to what it was for the first item. My one item tests already verify that the stack and head have returned to their initial state.
     */
    public function testAdjacentItemStack(){
        $this->disable();
    }

    /**
     * @test that $stack->stack is what we expect after each step
     */
    public function testOneItemStack(){
        $ItemInfo = (object)['name'=>'Blog'];
        $Item = ['title'=>'Some Post', 'description'=>'test desc'];

        $this->test('init stack');
        $stack = new \Phad\Stack();
        $tstack = [&$stack->data];
        $this->compare_raw($tstack, $stack->stack, true);

        $this->test('item started stack');
        $stack->itemListStarted($ItemInfo);
        $tstack[] = &$stack->data['BlogList'];
        $this->compare_raw($tstack, $stack->stack, true);

        $this->test('row started stack');
        $stack->rowStarted($ItemInfo, $Item);
        $tstack[] = &$stack->data['BlogList'][0];
        $this->compare_raw($tstack, $stack->stack, true);

        $this->test('row stopped stack');
        $stack->rowFinished($ItemInfo, $Item);
        array_pop($tstack);
        $this->compare_raw($tstack, $stack->stack, true);

        $this->test('item stopped stack');
        $stack->itemListFinished($ItemInfo, $Item);
        array_pop($tstack);
        $this->compare_raw($tstack, $stack->stack, true);


        $this->test('Final data');
        $this->compare_raw(
            ['BlogList'=>[$Item]],
            $stack->data,
        );
    }

    /**
     * @test that $stack->head is what we expect after each step
     *
     * 1. stack head is $stack->data
     * 2. start blog item. head is $stack->data['BlogList']
     * 3. start blog row. head is $stack->data['BlogList'][0]
     * 4. end blog row. head is $stack->data['BlogList']
     * 5. end blog item. head is $stack->data
     */
    public function testOneItemHead(){
        $ItemInfo = (object)['name'=>'Blog'];
        $Item = ['title'=>'Some Post', 'description'=>'test desc'];

        $this->test('init head');
        $stack = new \Phad\Stack();
        $thead = &$stack->data;
        $this->compare_raw($thead, $stack->head, true);

        $this->test('item started head');
        $stack->itemListStarted($ItemInfo);
        $thead = &$stack->data['BlogList'];
        $this->compare_raw($thead, $stack->head, true);

        $this->test('row started head');
        $stack->rowStarted($ItemInfo, $Item);
        $thead = &$stack->data['BlogList'][0];
        $this->compare_raw($thead, $stack->head, true);

        $this->test('row stopped stack');
        $stack->rowFinished($ItemInfo, $Item);
        $thead = &$stack->data['BlogList'];
        $this->compare_raw($thead, $stack->head, true);

        $this->test('item stopped stack');
        $stack->itemListFinished($ItemInfo, $Item);
        $thead = &$stack->data;
        $this->compare_raw($thead, $stack->head, true);

        $this->test('Final data');
        $this->compare_raw(
            ['BlogList'=>[$Item]],
            $stack->data,
        );
    }
}
